package controllers;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;
import views.MainView;

/**
 * Classe responsável por iniciar o processo do JavaFX e consequentemente toda a interface gráfica e views
 */
public class ApplicationController extends Application {
    private static ApplicationController instance; // instância singleton

    /**
     * Iniciador dessa interface do JavaFX
     * @param primaryStage palco de desenho
     * @throws Exception caso falhe a inicialização
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Platform.setImplicitExit(true); // fecha a aplicação quando fechar a janela
    }

    /**
     * Método responsável pela obtenção da instância singleton
     * @return instância singleton
     */
    public synchronized static ApplicationController getInstance() {
        if (instance == null) {
            (new Thread(() -> {
                instance = new ApplicationController();
                Application.launch(ApplicationController.class); // processo JavaFX rodará durante toda a aplicação
            })).start();
            try {
                while (instance == null)
                    Thread.sleep(100); // aguarda a inicialiação do processo JavaFX
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    /**
     * Inicia uma view qualquer
     * @param view view a ser iniciada
     * @param <T> objeto genérico da view que extenda ApplicationController
     */
    private <T extends ApplicationController> void startView(T view) {
        try{
            view.start(new Stage());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getMainView(){
        MainView mainView = MainView.getInstance();

        if(!mainView.hasStarted()){
            Platform.runLater(() -> startView(mainView)); // executa view na thread principal do javafx
        }

    }

    /**
     * Inicia a aplicação
     */
    public void run(){
        getMainView();
    }
}
