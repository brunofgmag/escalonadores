package cpus;

import escalonadores.BaseEscalonador;
import gerenciadores.BaseGerenciador;
import javafx.scene.layout.HBox;
import objects.Core;

import java.util.ArrayList;
import java.util.List;

public class CPU {
    private final List<Core> cores;

    public CPU(int numCores, BaseEscalonador escalonador, BaseGerenciador gerenciador, HBox coresBox){
        cores = new ArrayList<>();

        for(int i = 0; i < numCores; i++){
            cores.add(new Core(escalonador, gerenciador, coresBox, i));
        }
    }

    public List<Core> getCores(){
        return cores;
    }
}
