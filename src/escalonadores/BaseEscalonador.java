package escalonadores;

import objects.BaseProcesso;

public abstract class BaseEscalonador {
    public abstract BaseProcesso getProcesso(int numCore);
}
