package escalonadores;

import cpus.CPU;
import filas.ibs.BaseFila;
import filas.ibs.FilaPU;
import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import models.Estado;
import models.Processo;
import objects.BaseProcesso;
import objects.Core;
import objects.ProcessoIBS;

import java.util.*;

public class IBS extends BaseEscalonador {

    private List<BaseProcesso> ready;    //lista de processos a ser escalonado.
    private BaseFila[] filasCores;       //fila de processos que vão ser executados no core.
    private BaseFila running;            //fila de procesos que foram selecionados para ir par a fila de execução do core.
    private BaseFila remainder;          //fila de processos que não foram selecionados para serem executados.

    private final HBox[] listaCoresBox;  //HBox da lista de execução de filasCores.
    private final HBox esperaBox;        //HBox da lista de processos que não foram escalonados mas estão esperando serem executados.
    private final HBox bloqueadoBox;     //HBox da lista de processos que não foram selecionado para serem executados e que o tempo de ser executado já passou.
    private final HBox finalizadoBox;    //HBox da lista de processos que terminaram de ser executados.
    private final Label labelTime;       //HBox da contagem do tempo de execução do escalonador.

    private int numProcessos;            //número de procesos que o escalonador tem para tentar executar (todos os processos).
    private int lastCore;                //número do ultimo core que foi chamado para inserir processos.
    private int numCores;                //número de filasCores que o escalonador possui.
    private int[] lastEnd;               //valor do end do ultimo processo que foi escalonado/executado para o n core (quando ele escalona pela primeira vez o valor do lastEnd do core é o do ultimo processo que vai ser executado pelo core, mas a medida que o core vai executando o lastEnd recebe o end do processo que foi executado).

    private Time tempoExecutado;         //o tempo total que o escalonador tem de execução.
    private TextArea textArea;           //texto para mostrar o tempo.

    private BaseGerenciador gerenciador;

    private CPU cpu;

    private boolean loopRunning = true;



    public IBS(int numCores, Label labelTime, HBox[] listaCoresBox, HBox esperaBox, HBox bloqueadoBox, HBox finalizadoBox){

        this.lastEnd = new int[numCores];
        for(int i = 0; i < numCores; i++){
            this.lastEnd[i] = -1;
        }

        this.listaCoresBox = new HBox[numCores];
        System.arraycopy(listaCoresBox, 0, this.listaCoresBox, 0, listaCoresBox.length);

        this.bloqueadoBox = bloqueadoBox;
        this.finalizadoBox = finalizadoBox;
        this.labelTime = labelTime;
        this.esperaBox = esperaBox;

        this.running = new FilaPU();
        this.remainder = new FilaPU();
        this.ready = new ArrayList<>();

        this.tempoExecutado = new Time();
        this.textArea = new TextArea();

        this.numProcessos = 0;
        this.numCores = numCores;
        this.lastCore = -1;

    }

    /**
     * Método responsavel por pegar o processo de determinado core.
     *
     * Se o processo que for pegue do core ainda não for a hora dele executar o core vai receber null.
     *
     * Sempre que um processo for executado o lastEnd daquele core vai ser atualizado para o end do processo que foi executado.
     * Se o processo vier null então o lastEnd vai pra -1.
     *
     * */
    @Override
    public synchronized BaseProcesso getProcesso(int numCore) {

        return filasCores[numCore].getProcesso(tempoExecutado.getTime());
    }

    public void setCpu(CPU cpu){
        this.cpu = cpu;
    }

    private void getLastEndCore(){

        List<Core> core;

        core = cpu.getCores();

        for(int i = 0; i < numCores; i++){

            if(core.get(i).getProcesso() == null){
                lastEnd[i] = -1;
            }else{
                lastEnd[i] = core.get(i).getProcesso().getEnd();
            }


        }

    }

    public void addProcessoEstatico(){
        BaseProcesso processo = new ProcessoIBS(numProcessos++, tempoExecutado, esperaBox, bloqueadoBox, finalizadoBox, gerenciador, Processo.ESTATICO);
        ready.add(processo);
    }

    public synchronized void addProcessoDinamico(){

        BaseProcesso processo = new ProcessoIBS(numProcessos++, tempoExecutado, esperaBox, bloqueadoBox, finalizadoBox, gerenciador, Processo.DINAMICO);
        ready.add(processo);
        for (BaseFila core : filasCores) {

            ready.addAll(core.getProcessosSetEspera());

        }

        for(int  i = 0; i < ready.size(); i++){

            ready.get(i).setIdCore(-1);
            if(ready.get(i).getEstado() == Estado.FINALIZADO || ready.get(i).getEstado() == Estado.ABORTADO || ready.get(i).getEstado() == Estado.EXECUTANDO){
                ready.remove(i);
                i--;
            }

        }

        lastCore = -1;

        getLastEndCore();

//        for(int i = 0; i < lastEnd.length; i++ ){
//            lastEnd[i] = tempoExecutado.getTime();
//        }

        escalonar();

    }

    public synchronized void escalonar(){

        lastCore++;

        if(lastCore < filasCores.length){

            sort();

            for (BaseProcesso processo: ready) {

                if(processo.getStart() >= lastEnd[lastCore]){
                    if(processo.getEstado() != Estado.ABORTADO){
                        setLastEnd(lastCore, processo.getEnd());
                        processo.setIdCore(lastCore);
                        running.put(processo);
                        ((ProcessoIBS)processo).setPronto(listaCoresBox[lastCore]);
                    }
                }else{
                    if(processo.getEstado() != Estado.ABORTADO){
                        processo.setIdCore(-1);
                        remainder.put(processo);
                        ((ProcessoIBS)processo).setEmEspera();
                    }
                }

            }

            filasCores[lastCore].addAll(running.getProcessos());

            ready = new ArrayList<>();

            ready.addAll(remainder.getProcessos());

            sortGui(listaCoresBox[lastCore]);

            escalonar();
        }else {
            lastCore = -1;
        }
    }

    private void sort(){

        ObservableList<BaseProcesso> workingCollection = FXCollections.observableArrayList(ready);

        workingCollection.sort(new ProcessoComparator());

        ready = workingCollection;

    }

    private void sortGui(HBox hBox){
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    try {
                        ObservableList<Node> workingCollection = FXCollections.observableArrayList(hBox.getChildren());

                        workingCollection.sort(new NodeComparator());

                        hBox.getChildren().setAll(workingCollection);
                    } catch (Exception e) {
                        //do nothing
                    }
                });
            }
        }, 100);
    }

    public void setNumCores(int numCores){

        this.filasCores = new FilaPU[numCores];

        for(int i = 0; i < filasCores.length; i++){
            filasCores[i] = new FilaPU();
        }

    }

    private void setLastEnd(int numCore, int lastEnd){
        this.lastEnd[numCore] = lastEnd;
    }

    public void startTimer(){

        textArea.setPrefRowCount(3);
        textArea.setPrefColumnCount(5);
        textArea.setEditable(false);

        new Thread(() -> {
            try {
                while(loopRunning){
                    Thread.sleep(1000);
                    tempoExecutado.incTime();
                    Platform.runLater(() -> labelTime.setText("Tempo de execução: " + tempoExecutado.getTime()));
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    /**
     * Comparador personalizado de nodes do JavaFX, compara os nodes para ordenação
     */
    private static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(Node o1, Node o2) {
            TextArea t1 = (TextArea) o1;

            TextArea t2 = (TextArea) o2;

            String line1[] = t1.getText().split("\\r\\n|\\n|\\r");
            String value1[] = line1[1].split(" ");

            String line2[] = t2.getText().split("\\r\\n|\\n|\\r");
            String value2[] = line2[1].split(" ");

            if(Integer.valueOf(value1[1]) > Integer.valueOf(value2[1])) {
                return 1;
            } else if (Integer.valueOf(value1[1]) < Integer.valueOf(value2[1])) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    private static class ProcessoComparator implements Comparator<BaseProcesso> {
        @Override
        public int compare(BaseProcesso o1, BaseProcesso o2) {

            int start1 = o1.getStart();
            int end1 = o1.getEnd();

            int start2 = o2.getStart();
            int end2 = o2.getEnd();

            if (end1 < end2) {
                return -1;
            }
            if (end1 > end2) {
                return 1;
            }
            if(end1 == end2){
                if(start1 < start2){
                    return -1;
                }else{
                    return 1;
                }
            }
            return 0;


        }
    }

    public class Time {

        private int tempo;

        Time(){

            tempo = 0;
        }

        void incTime(){

            tempo++;

        }

        public int getTime() {
            return tempo;
        }

    }

    public void setGerenciador(BaseGerenciador gerenciador) {
        this.gerenciador = gerenciador;
    }
}
