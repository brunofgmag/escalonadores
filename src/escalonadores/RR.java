package escalonadores;

import filas.rr.*;
import gerenciadores.BaseGerenciador;
import javafx.scene.layout.HBox;
import models.Processo;
import objects.BaseProcesso;
import objects.ProcessoRR;

public class RR extends BaseEscalonador {
    private final BaseFila[] filas;
    private final HBox p0box;
    private final HBox p1box;
    private final HBox p2box;
    private final HBox p3box;
    private final HBox finalizadosBox;
    private final HBox abortadosBox;
    private int quantum;
    private int numProcessos;
    private int index = 0;
    private BaseGerenciador gerenciador;

    public RR(HBox p0box, HBox p1box, HBox p2box, HBox p3box, HBox abortadosBox, HBox finalizadosBox){
        this.filas = new BaseFila[4];
        this.p0box = p0box;
        this.p1box = p1box;
        this.p2box = p2box;
        this.p3box = p3box;
        this.finalizadosBox = finalizadosBox;
        this.abortadosBox = abortadosBox;

        filas[0] = new FilaP0();
        filas[1] = new FilaP1();
        filas[2] = new FilaP2();
        filas[3] = new FilaP3();
    }

    @Override
    public synchronized BaseProcesso getProcesso(int numCores) {
        if(index == 4){
            index = 0;
        }

        BaseProcesso p = filas[index].getProcesso();

        if(p != null){
            switch (p.getPrioridade()){
                case 0: p.setQuantum(quantum * 4);
                break;
                case 1: p.setQuantum(quantum * 3);
                break;
                case 2: p.setQuantum(quantum * 2);
                break;
                case 3: p.setQuantum(quantum);
            }
        }

        index++;

        return p;
    }

    /**
     * Adiciona processo de volta a sua fila de prioridade
     * @param processo processo a ser colocado na fila
     */
    public synchronized void putBack(BaseProcesso processo){
        filas[processo.getPrioridade()].put(processo);
    }

    /**
     * Adiciona um processo uma vez que o escalonador começa a rodar e antes da CPU iniciar
     */
    public void addProcessoEstatico(){
        BaseProcesso processo = new ProcessoRR(numProcessos++, p0box, p1box, p2box, p3box, finalizadosBox, abortadosBox, gerenciador, this, models.Processo.ESTATICO);
        filas[processo.getPrioridade()].put(processo);
    }

    /**
     * Adiciona um processo dinamicamente
     */
    public void addProcessoDinamico(){
        BaseProcesso processo = new ProcessoRR(numProcessos++, p0box, p1box, p2box, p3box, finalizadosBox, abortadosBox, gerenciador, this, Processo.DINAMICO);
        filas[processo.getPrioridade()].put(processo);
    }

    public void setQuantum(int quantum) {
        this.quantum = quantum;
    }

    public void setGerenciador(BaseGerenciador gerenciador) {
        this.gerenciador = gerenciador;
    }
}
