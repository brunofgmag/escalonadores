package escalonadores;

import filas.sjf.FilaPU;
import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import objects.BaseProcesso;
import objects.ProcessoSJF;

import java.util.*;

public class SJF extends BaseEscalonador{
    private final FilaPU fila;
    private int numProcessos;
    private final HBox aptosBox;
    private final HBox finalizadosBox;
    private final HBox abortadosBox;
    private BaseGerenciador gerenciador;

    public SJF(HBox aptosBox, HBox finalizadosBox, HBox abortadosBox){
        fila = new FilaPU();

        this.abortadosBox = abortadosBox;
        this.aptosBox = aptosBox;
        this.finalizadosBox = finalizadosBox;
    }

    @Override
    public synchronized BaseProcesso getProcesso(int numCores){
        return fila.getProcesso();
    }

    /**
     * Adiciona um processo uma vez que o escalonador começa a rodar e antes da CPU iniciar
     */
    public void addProcessoEstatico(){
        BaseProcesso processo = new ProcessoSJF(numProcessos++, aptosBox, finalizadosBox, abortadosBox, gerenciador, models.Processo.ESTATICO);
        fila.put(processo);
    }

    /**
     * Adiciona um processo dinamicamente
     */
    public void addProcessoDinamico(){
        BaseProcesso processo = new ProcessoSJF(numProcessos++, aptosBox, finalizadosBox, abortadosBox, gerenciador, models.Processo.DINAMICO);
        fila.put(processo);
    }

    /**
     * Método de ordenação da box de aptos
     */
    public void sortViewBox(){
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    try {
                        ObservableList<Node> workingCollection = FXCollections.observableArrayList(aptosBox.getChildren());

                        workingCollection.sort(new NodeComparator());

                        aptosBox.getChildren().setAll(workingCollection);
                    } catch (Exception e) {
                        //do nothing
                    }
                });
            }
        }, 500);

    }

    /**
     * Comparador personalizado de nodes do JavaFX, compara os nodes para ordenação
     */
    private static class NodeComparator implements Comparator<Node> {
        @Override
        public int compare(Node o1, Node o2) {
            TextArea t1 = (TextArea) o1;

            TextArea t2 = (TextArea) o2;

            String line1[] = t1.getText().split("\\r\\n|\\n|\\r");
            String value1[] = line1[1].split(" ");

            String line2[] = t2.getText().split("\\r\\n|\\n|\\r");
            String value2[] = line2[1].split(" ");

            if(Integer.valueOf(value1[1]) > Integer.valueOf(value2[1])) {
                return 1;
            } else if (Integer.valueOf(value1[1]) < Integer.valueOf(value2[1])) {
                return -1;
            } else {
                return 0;
            }
        }
    }

    public void setGerenciador(BaseGerenciador gerenciador) {
        this.gerenciador = gerenciador;
    }
}