package filas.ibs;

import objects.BaseProcesso;

import java.util.List;

public abstract class BaseFila {

    public abstract void put(BaseProcesso processo);

    public abstract BaseProcesso getProcesso(int time);

    public abstract void limparFila();

    public abstract void addAll(List<BaseProcesso> processos);

    public abstract List<BaseProcesso> getProcessos();

    public abstract List<BaseProcesso> getProcessosSetEspera();
}
