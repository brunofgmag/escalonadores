package filas.ibs;

import objects.BaseProcesso;
import objects.ProcessoIBS;

import java.util.ArrayList;
import java.util.List;

/**
 * Fila de prioridade única para IBS
 */
public class FilaPU extends BaseFila{

    private List<BaseProcesso> lista;

    public FilaPU() {

        this.lista = new ArrayList<>();

    }

    @Override
    public void put(BaseProcesso processo) {
        lista.add(processo);
    }

    @Override
    public BaseProcesso getProcesso(int time) {
        if(lista.isEmpty()){
            return null;
        }else if(lista.get(0).getStart() == time){
            BaseProcesso p = lista.get(0);
            lista.remove(0);
            return p;
        }else{
            return null;
        }
    }

    @Override
    public List<BaseProcesso> getProcessos(){

        List<BaseProcesso> processos = new ArrayList<>();
        processos.addAll(lista);
        limparFila();
        return processos;

    }

    @Override
    public List<BaseProcesso> getProcessosSetEspera() {
        List<BaseProcesso> processos = new ArrayList<>();
        processos.addAll(lista);
        for(BaseProcesso processo : lista){
            ((ProcessoIBS) processo).setEmEspera();
        }
        limparFila();
        return processos;
    }

    @Override
    public void limparFila() {
        this.lista = new ArrayList<>();
    }

    @Override
    public void addAll(List<BaseProcesso> processos){
        lista.addAll(processos);
    }


}

