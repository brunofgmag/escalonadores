package filas.ltg;

import objects.BaseProcesso;

/**
 * Fila de Prioridade única para SJF
 */
public class FilaPU {
    private BaseProcesso header;
    private int numProcessos = 0;

    public void put(BaseProcesso processo){
        if(isEmpty()){
            header = processo;
            numProcessos++;
        } else {
            BaseProcesso p = header;

            if(header.getNext() == null){
                if(processo.getDeadline() > p.getDeadline()){
                    p.setNext(processo);

                    numProcessos++;
                } else {
                    processo.setNext(header);
                    header = processo;

                    numProcessos++;
                }
            } else {
                if(processo.getDeadline() < header.getDeadline()){
                    processo.setNext(header);
                    header = processo;

                    numProcessos++;

                    return;
                }

                while (p.getNext() != null){
                    if(processo.getDeadline() < p.getNext().getDeadline()){
                        BaseProcesso temp = p.getNext();
                        p.setNext(processo);
                        processo.setNext(temp);

                        numProcessos++;

                        return;
                    } else {
                        p = p.getNext();
                    }
                }

                p.setNext(processo);

                numProcessos++;
            }
        }
    }

    public BaseProcesso getProcesso(){
        if(isEmpty()){
            return null;
        } else {
            if(header.getNext() != null){
                BaseProcesso p = header;
                header = header.getNext();

                numProcessos--;

                return p;
            } else {
                BaseProcesso p = header;
                header = null;

                numProcessos--;

                return p;
            }
        }
    }

    private boolean isEmpty(){
        return numProcessos == 0;
    }
}