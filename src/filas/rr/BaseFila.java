package filas.rr;

import objects.BaseProcesso;

public abstract class BaseFila {
    public abstract void put(BaseProcesso processo);

    public abstract BaseProcesso getProcesso();
}
