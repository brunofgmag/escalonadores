package filas.rr;

import objects.BaseProcesso;

import java.util.LinkedList;
import java.util.Queue;

public class FilaP0 extends BaseFila {
    private Queue<BaseProcesso> processos = new LinkedList<>();

    @Override
    public void put(BaseProcesso processo) {
        processos.add(processo);
    }

    @Override
    public BaseProcesso getProcesso() {
        return processos.poll();
    }
}
