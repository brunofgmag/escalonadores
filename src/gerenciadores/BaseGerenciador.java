package gerenciadores;

import objects.BaseBloco;
import objects.BaseProcesso;

/**
 * Created by Bruno on 16/11/2017.
 * Escalonadores
 */
public abstract class BaseGerenciador {
    public abstract boolean alocarMemoria(int quantidadeMemoria, BaseProcesso processo);
    public abstract void desalocarMemoria(BaseProcesso processo);
}
