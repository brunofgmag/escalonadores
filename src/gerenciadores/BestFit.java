package gerenciadores;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import objects.BaseBloco;
import objects.BaseProcesso;
import objects.BlocoBestFit;

/**
 * Created by Bruno on 21/11/2017.
 * Escalonadores
 */
public class BestFit extends BaseGerenciador {
    private final HBox blocosMemoriaBox;
    private BaseBloco headerLivres;
    private BaseBloco header;
    private int memoriaNaoAlocada;
    private int memoriaAlocada;
    private int memoriaEmUso;
    private final Label mnaLabel;
    private final Label maLabel;
    private final Label meuLabel;
    private int qtdeBlocos = 0;

    public BestFit(int memoriaNaoAlocada, HBox blocosMemoriaBox, Label mnaLabel, Label maLabel, Label meuLabel)
    {
        this.maLabel = maLabel;
        this.meuLabel = meuLabel;
        this.mnaLabel = mnaLabel;
        this.memoriaNaoAlocada = memoriaNaoAlocada;
        this.blocosMemoriaBox = blocosMemoriaBox;

        this.mnaLabel.setText("Memória não alocada: " + memoriaNaoAlocada + "KB");
    }

    @Override
    public synchronized boolean alocarMemoria(int tamanhoRequisicao, BaseProcesso processo) {
        int valorBestFit = 1024;
        BaseBloco temp = headerLivres;
        BaseBloco ponteiroBestFit = null;

        if(temp != null)
        {
            if(memoriaNaoAlocada >= tamanhoRequisicao)
            {
                memoriaNaoAlocada -= tamanhoRequisicao;
                memoriaAlocada += tamanhoRequisicao;
                memoriaEmUso += tamanhoRequisicao;
                criarBloco(tamanhoRequisicao, processo);
                return true;
            }

            if(temp.getProximoBlocoLivre() == null)
            {
                if(temp.getTamanhoBloco() >= tamanhoRequisicao)
                {
                    memoriaEmUso += tamanhoRequisicao;
                    temp.alocarBloco(processo, tamanhoRequisicao);
                    removerBlocoLivre(temp);
                    atualizarMemoriaEmUso();
                    return true;
                }
            }

            while (temp.getProximoBlocoLivre() != null)
            {
                if(temp.getTamanhoBloco() == tamanhoRequisicao)
                {
                    memoriaEmUso += tamanhoRequisicao;
                    temp.alocarBloco(processo, tamanhoRequisicao);
                    removerBlocoLivre(temp);
                    atualizarMemoriaEmUso();
                    return true;
                }
                else
                {
                    if(temp.getTamanhoBloco() > tamanhoRequisicao)
                    {
                        if(temp.getTamanhoBloco() <= valorBestFit)
                        {
                            valorBestFit = temp.getTamanhoBloco();
                            ponteiroBestFit = temp;
                        }
                    }
                }

                temp = temp.getProximoBlocoLivre();
            }

            if(temp.getTamanhoBloco() > tamanhoRequisicao)
            {
                if(temp.getTamanhoBloco() <= valorBestFit)
                {
                    ponteiroBestFit = temp;
                }
            }

            if(ponteiroBestFit != null)
            {
                memoriaEmUso += tamanhoRequisicao;
                ponteiroBestFit.alocarBloco(processo, tamanhoRequisicao);
                removerBlocoLivre(ponteiroBestFit);
                atualizarMemoriaEmUso();
                return true;
            }
        }
        else
        {
            if(memoriaNaoAlocada >= tamanhoRequisicao)
            {
                memoriaEmUso += tamanhoRequisicao;
                memoriaNaoAlocada -= tamanhoRequisicao;
                memoriaAlocada += tamanhoRequisicao;
                criarBloco(tamanhoRequisicao, processo);
                atualizarMemoriaEmUso();
                return true;
            }
            else
            {
                return false;
            }
        }

        return false;
    }

    @Override
    public synchronized void desalocarMemoria(BaseProcesso processo) {
        new Thread(() -> {
            BaseBloco temp = header;

            if(temp != null && processo != null)
            {
                if(header.getProcessoEmUso() == processo)
                {
                    header.desalocar();
                    adicionarBlocoLivre(header);
                }

                temp = temp.getProximo();

                while(temp.getProximo() != null && temp != header)
                {
                    if (temp.getProcessoEmUso() == processo)
                    {
                        temp.desalocar();
                        adicionarBlocoLivre(temp);
                    }

                    temp = temp.getProximo();
                }

                memoriaEmUso += temp.getTamanhoEmUso();

                if(temp.getProcessoEmUso() == processo)
                {
                    temp.desalocar();
                    adicionarBlocoLivre(temp);
                }
            }

            atualizarMemoriaEmUso();
        }).start();
    }

    private void criarBloco(int tamanhoBloco, BaseProcesso processo)
    {
        atualizarMemoriaEmUso();
        Platform.runLater(() -> {
            mnaLabel.setText("Memória não alocada: " + memoriaNaoAlocada + "KB");
            maLabel.setText("Memória alocada: " + memoriaAlocada + "KB");
        });
        BaseBloco bloco = new BlocoBestFit(qtdeBlocos++, tamanhoBloco, processo, blocosMemoriaBox);

        if(header == null)
        {
            header = bloco;
        }
        else
        {
            BaseBloco temp = header;

            while(temp.getProximo() != null)
            {
                temp = temp.getProximo();
            }

            temp.setProximo(bloco);
        }
    }

    private boolean isPresent(BaseBloco bloco)
    {
        BaseBloco temp = headerLivres;

        if(temp == null)
        {
            return false;
        }

        while (temp.getProximoBlocoLivre() != null)
        {
            if(temp == bloco)
            {
                return true;
            }

            temp = temp.getProximoBlocoLivre();
        }

        return temp == bloco;

    }

    private void adicionarBlocoLivre(BaseBloco bloco) {
        if(isPresent(bloco))
        {
            return;
        }

        if(headerLivres == null)
        {
            headerLivres = bloco;
            desalocarMemoria(headerLivres.getProcessoEmUso());
        }
        else
        {
            desalocarMemoria(headerLivres.getProcessoEmUso());
            bloco.setProximoBlocoLivre(headerLivres);
            headerLivres = bloco;
        }
    }

    private void removerBlocoLivre(BaseBloco bloco)
    {
        BaseBloco temp = headerLivres;

        if(temp != null)
        {
            if(headerLivres == bloco)
            {
                headerLivres = bloco.getProximoBlocoLivre();
                return;
            }

            while(temp.getProximoBlocoLivre() != null)
            {
                if (temp.getProximoBlocoLivre() == bloco)
                {
                    temp.setProximoBlocoLivre(temp.getProximoBlocoLivre().getProximoBlocoLivre());

                    return;
                }

                temp = temp.getProximoBlocoLivre();
            }
        }
    }

    private void atualizarMemoriaEmUso()
    {
        memoriaEmUso = 0;
        BaseBloco temp = header;

        if(temp != null)
        {
            while (temp.getProximo() != null)
            {
                memoriaEmUso += temp.getTamanhoEmUso();
                temp = temp.getProximo();
            }

            memoriaEmUso += temp.getTamanhoEmUso();

            Platform.runLater(() -> {
                meuLabel.setText("Memória em uso: " + memoriaEmUso + "KB");
            });
        }
    }
}
