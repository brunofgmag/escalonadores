package gerenciadores;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import objects.BaseBloco;
import objects.BaseProcesso;
import objects.BlocoMergeFit;

/**
 * Created by Daniel on 22/11/2017.
 */

public class MergeFit extends BaseGerenciador{

    private final HBox memoriaBox;

    private BaseBloco headerMergeFit;
    private BaseBloco headerLivres;

    private Label labelMemoriaNA;
    private Label labelMemoriaAlocada;
    private Label labelMemoriaEmUso;

    private int memoriaTotal;
    private int memoriaNaoAlocada;
    private int memoriaAlocada;
    private int memoriaEmUso;
    private int qtdBlocos = 0;


    public MergeFit(int memoriaTotal, HBox memoriaBox, Label labelMemoriaNA, Label labelMemoriaAlocada, Label labelMemoriaEmUso){

        this.memoriaBox = memoriaBox;
        this.memoriaTotal = memoriaTotal;
        this.labelMemoriaAlocada = labelMemoriaAlocada;
        this.labelMemoriaEmUso = labelMemoriaEmUso;
        this.labelMemoriaNA = labelMemoriaNA;

        refreshLabels();

    }

    @Override
    public synchronized boolean alocarMemoria(int tamanhoRequisicao, BaseProcesso processo) {

        merge();

        if(tamanhoRequisicao > memoriaTotal){

            return false;

        }else if(headerMergeFit == null){

            BaseBloco bloco = new BlocoMergeFit(qtdBlocos++, memoriaTotal, null, memoriaBox, true);
            headerMergeFit = bloco;
            adicionarBlocoLivre(bloco);

            bloco = split(bloco,tamanhoRequisicao,0);
            bloco.alocarBloco(processo, tamanhoRequisicao);

            refreshLabels();

            return true;

        }else{

            BaseBloco temp = headerMergeFit;
            int index = 0;

            while(temp != null){

                if(temp.getTamanhoBloco() >= tamanhoRequisicao){

                    BaseBloco bloco = split(temp, tamanhoRequisicao, index);
                    bloco.alocarBloco(processo,tamanhoRequisicao);

                    refreshLabels();

                    return true;

                }else{

                    temp = temp.getProximo();

                }

                index++;

            }

            return false;

        }

    }

    @Override
    public synchronized void desalocarMemoria(BaseProcesso processo) {

        new Thread(()->{

            if(processo != null){

                BaseBloco temp = headerMergeFit;

                while(temp != null){

                    if(temp.getProcessoEmUso() == processo){

                        temp.desalocar();
                        adicionarBlocoLivre(temp);

                    }

                    temp = temp.getProximo();

                }
                merge();
                refreshLabels();

            }

        }).start();



    }

    public void startMerge(){

        new Thread(()->{

            try {

                while(true){

                    Thread.sleep(1000);
                    merge();

                }

            }catch (InterruptedException e){
                e.printStackTrace();
            }


        }).start();

    }

    public synchronized void merge(){


        BaseBloco temp = headerLivres;

        while(temp != null){

            if(temp.getProximo() != null && temp.getProximo().isEmpty()){

                removeFromMemoriaBox(temp.getProximo().getTextArea());
                removerBlocoLivre(temp.getProximo());

                temp.setTamanhoBloco(temp.getTamanhoBloco() + temp.getProximo().getTamanhoBloco());
                temp.setProximo(temp.getProximo().getProximo());

                temp.refreshTextArea();


            }else{

                temp = temp.getProximoBlocoLivre();

            }


        }


    }


    /**
     * > O split sempre vai ser chamado quando for adcionar um processo na HBox de execução.
     * > Para o metodo funcinar o parâmetro memoriaUtilizada tem que ser maior ou igual ao tamanho
     * do bloco.
     * > O split vai retornar um bloco no ponto de somente adcionar o processo nele, ou seja, já vai adcionar o resto
     * (se houver) do split na lista de livres e o bloco retornado vai ser removido da lista de livres.
     * */

    public BaseBloco split(BaseBloco bloco, int tamanhoRequisicao, int index){

        if(bloco.getTamanhoBloco() == tamanhoRequisicao){

            removerBlocoLivre(bloco);

            if(memoriaTotal != 0){
                addMemoriaBox(index, bloco);
            }

            return bloco;

        }else{

            BaseBloco splittedBloco;

            if(bloco.getProximo() == null){
                splittedBloco = new BlocoMergeFit(qtdBlocos++, bloco.getTamanhoBloco() - tamanhoRequisicao, null, memoriaBox, false);
            }else{
                splittedBloco = new BlocoMergeFit(generateBlocoId(bloco), bloco.getTamanhoBloco() - tamanhoRequisicao, null, memoriaBox, false);
            }

            bloco.setTamanhoBloco(tamanhoRequisicao);

            splittedBloco.setProximo(bloco.getProximo());
            bloco.setProximo(splittedBloco);

            adicionarBlocoLivre(splittedBloco);
            removerBlocoLivre(bloco);

            addMemoriaBox(index + 1, splittedBloco);

            merge();
            return bloco;

        }

    }




    public void adicionarBlocoLivre(BaseBloco bloco) {

        if(headerLivres == null){

            headerLivres = bloco;

        }else{

            BaseBloco temp = headerLivres;

            while(temp.getProximoBlocoLivre() != null){
                temp = temp.getProximoBlocoLivre();
            }

            temp.setProximoBlocoLivre(bloco);

        }

        bloco.setProximoBlocoLivre(null);

    }

    public void removerBlocoLivre(BaseBloco bloco) {

        BaseBloco temp = headerLivres;

        if(temp != null){

            if(headerLivres == bloco){
                headerLivres = bloco.getProximoBlocoLivre();
                return;
            }

            while(temp.getProximoBlocoLivre() != null){

                if(temp.getProximoBlocoLivre() == bloco){
                    temp.setProximoBlocoLivre(temp.getProximoBlocoLivre().getProximoBlocoLivre());
                    return;
                }

                temp = temp.getProximoBlocoLivre();

            }

        }

    }

    public void refreshLabels(){

        memoriaAlocada = 0;
        memoriaEmUso = 0;
        memoriaNaoAlocada = 0;

        BaseBloco temp = headerMergeFit;

        while(temp != null){

            if(temp.getProcessoEmUso() != null){
                memoriaEmUso += temp.getTamanhoBloco();
            }

            memoriaAlocada += temp.getTamanhoBloco();

            temp = temp.getProximo();

        }

        memoriaNaoAlocada = memoriaTotal - memoriaAlocada;

        Platform.runLater(() -> {

            labelMemoriaNA.setText("Memória não alocada: " + memoriaNaoAlocada + "KB");
            labelMemoriaAlocada.setText("Memória alocada: " + memoriaAlocada + "KB");
            labelMemoriaEmUso.setText("Memória em uso: " + memoriaEmUso + "KB");

        });

    }

    private void removeFromMemoriaBox(TextArea textArea){

        Platform.runLater(()->{

            memoriaBox.getChildren().remove(textArea);

        });


    }

    private void addMemoriaBox(int index, BaseBloco bloco){

        Platform.runLater(()->{

            if(!memoriaBox.getChildren().contains(bloco.getTextArea())){
                memoriaBox.getChildren().add(index, bloco.getTextArea());
            }

        });

    }

    private double generateBlocoId(BaseBloco bloco){

        return ((bloco.getIdBloco() + bloco.getProximo().getIdBloco()) / 2 );

    }

}
