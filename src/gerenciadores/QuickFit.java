package gerenciadores;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import objects.BaseBloco;
import objects.BaseProcesso;
import objects.BlocoQuickFit;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Daniel on 01/12/2017.
 */

public class QuickFit extends BaseGerenciador{

    private final HBox memoriaBox;

    private BaseBloco headerQuickFit;
    private BaseBloco headerLivres;
    private List<BaseBloco> quickList;

    private Label labelMemoriaNA;
    private Label labelMemoriaAlocada;
    private Label labelMemoriaEmUso;
    private Label labelBlocosQuick;

    private int memoriaTotal;
    private int memoriaNaoAlocada;
    private int memoriaAlocada;
    private int memoriaEmUso;
    private int qtdBlocos = 0;

    private int qtdFilas;
    private int qtdProcessos;
    private int qtdRequisições = 0;
    private List<Requisicao> requisicoes;




    public QuickFit(int memoriaTotal, HBox memoriaBox, Label labelMemoriaNA, Label labelMemoriaAlocada, Label labelMemoriaEmUso, Label labelBlocosQuick, int qtdProcessos, int qtdFilas){

        this.memoriaTotal = memoriaTotal;
        this.memoriaBox = memoriaBox;
        this.labelMemoriaAlocada = labelMemoriaAlocada;
        this.labelMemoriaEmUso = labelMemoriaEmUso;
        this.labelMemoriaNA = labelMemoriaNA;
        this.labelBlocosQuick = labelBlocosQuick;
        this.qtdFilas = qtdFilas;
        this.qtdProcessos = qtdProcessos;

        memoriaNaoAlocada = memoriaTotal;
        requisicoes = new ArrayList<>();

        criaQuickList();
    }

    @Override
    public synchronized boolean alocarMemoria(int quantidadeMemoria, BaseProcesso processo) {

        atualizaEstatistica(quantidadeMemoria);

        if(qtdRequisições == qtdProcessos){

            qtdRequisições = 0;
            atualizaQuickList();

        }

        if(memoriaTotal < quantidadeMemoria){//pouca mémoria inicial.

            return false;

        }else if(headerQuickFit == null){//1ª execução, não tem processos no gerenciador.

            criarBlocoNovo(quantidadeMemoria, processo);
            return true;

        }else {//não é primeira execução

            if(isBlocoQuick(quantidadeMemoria)){

                if(tryAlocarBlocoQuick(quantidadeMemoria, processo)){
                    return true;
                }else if(memoriaNaoAlocada >= quantidadeMemoria){
                    criarBlocoNovo(quantidadeMemoria, processo);
                    return true;
                }else if(tryAlocarBLocoLivre(quantidadeMemoria, processo)){
                    return true;
                }else{
                    return false;
                }

            }else{

                if(memoriaNaoAlocada >= quantidadeMemoria){
                    criarBlocoNovo(quantidadeMemoria, processo);
                    return true;
                }else if(tryAlocarBLocoLivre(quantidadeMemoria, processo)){
                    return true;
                }else if(tryAlocarBlocoQuick(quantidadeMemoria, processo)){
                    return true;
                }else{
                    return false;
                }

            }

        }

    }

    @Override
    public synchronized void desalocarMemoria(BaseProcesso processo) {

        BaseBloco temp = headerQuickFit;

        while(temp != null){

            if(temp.getProcessoEmUso() == processo){

                memoriaEmUso -= temp.getTamanhoEmUso();
                temp.desalocar();
                temp.setProximoBlocoLivre(null);

                if(isBlocoQuick(temp.getTamanhoBloco())){
                    adcionarBLocoQuickList(temp);
                }else{
                    adcionarBlocoLivre(temp);
                }


            }

            temp = temp.getProximo();

        }

        refreshLabels();

    }

    private void atualizaEstatistica(int tamanhoReq){

        qtdRequisições++;

        if(requisicoes.size() == 0){

            requisicoes.add(new Requisicao(tamanhoReq, colorGenerator()));

        }else{

            boolean isInserted = false;

            for (Requisicao requisicao : requisicoes) {

                if(requisicao.getTamanhoRequisicao() == tamanhoReq){
                    requisicao.incRequisicao();
                    isInserted = true;
                }

            }

            if (!isInserted){

                requisicoes.add(new Requisicao(tamanhoReq, colorGenerator()));
            }

        }

    }

    private void criaQuickList(){

        quickList = new ArrayList<>();

        for(int i = 0; i < qtdFilas; i++){

            quickList.add(new BlocoQuickFit(0));

        }

    }

    /**
     * > Verifica se uma requisição pode ir para um blocoQuick
     *
     * - return true se puder ir para um bloco quick.
     * - return false se não puder ir para um block quick.
     *
     * */
    private boolean isBlocoQuick(int tamanhoReq){

        for (BaseBloco bloco : quickList) {

            if(bloco.getTamanhoBloco() == tamanhoReq){
                return true;
            }

        }

        return false;

    }

    private void atualizaQuickList(){//vai atualizar de acordo com o numero de requisições mais feitas

        Collections.sort(requisicoes);

        int qtdRequisicoes = requisicoes.size();

        for(int i = 0; i < quickList.size(); i++){

            if(i >= qtdRequisicoes){

                quickList.get(i).setTamanhoBloco(0);

                BaseBloco temp = quickList.get(i);

                while(temp.getProximo()!= null){

                    BaseBloco bloco = temp.getProximo();

                    temp.setProximo(temp.getProximo().getProximoBlocoLivre());
                    adcionarBlocoLivre(bloco);

                }

            }else{

                quickList.get(i).setTamanhoBloco(requisicoes.get(i).getTamanhoRequisicao());

                BaseBloco temp = quickList.get(i);

                while(temp.getProximo()!= null){

                    BaseBloco bloco = temp.getProximo();

                    temp.setProximo(temp.getProximo().getProximoBlocoLivre());
                    adcionarBlocoLivre(bloco);

                }

                BaseBloco livres = headerLivres;

                while(livres != null){//adcioanando blocos livres na quickList

                    if(livres.getTamanhoBloco() == requisicoes.get(i).getTamanhoRequisicao()){

                        adcionarBLocoQuickList(livres);
                        removerBlocoLivre(livres);

                    }

                    livres = livres.getProximoBlocoLivre();

                }

            }

        }

        refreshLabelsBlocosQuick();
        refreshColorBox();

    }

    private boolean tryAlocarBlocoQuick(int tamanhoReq, BaseProcesso processo){

        BaseBloco temp;

        for (BaseBloco bloco : quickList) {

            if(bloco.getTamanhoBloco() == tamanhoReq && bloco.getProximo() != null){

                temp = bloco.getProximo();
                temp.alocarBloco(processo, tamanhoReq);
                removerBlocoQuickList(temp);
                temp.refreshTextAreaColor(getCorBloco(bloco.getTamanhoBloco()));

                refreshLabels();
                return true;

            }

        }

        return false;

    }

    private boolean tryAlocarBLocoLivre(int tamanhoReq, BaseProcesso processo){

        int valorQuickFit = 1024;
        BaseBloco ponteiro = null;
        BaseBloco temp = headerLivres;

        while(temp != null){

            if((temp.getTamanhoBloco() >= tamanhoReq) && (temp.getTamanhoBloco() < valorQuickFit)){

                valorQuickFit = temp.getTamanhoBloco();
                ponteiro = temp;

            }

            temp = temp.getProximoBlocoLivre();

        }

        if(ponteiro != null){

            removerBlocoLivre(ponteiro);
            ponteiro.alocarBloco(processo, tamanhoReq);

            if(isBlocoQuick(ponteiro.getTamanhoBloco())){
                ponteiro.refreshTextAreaColor(getCorBloco(ponteiro.getTamanhoBloco()));
            }else{
                ponteiro.refreshTextAreaColor(BlocoQuickFit.BASE_COR_OCUPADO);
            }

            refreshLabels();

            return true;

        }else{

            return false;

        }

    }

    private void criarBlocoNovo(int tamanhoReq, BaseProcesso processo){

        BaseBloco bloco = new BlocoQuickFit(qtdBlocos++, tamanhoReq, processo, memoriaBox);

        if(headerQuickFit == null){

            headerQuickFit = bloco;

        }else{

            BaseBloco temp = headerQuickFit;

            while(temp != null){

                if(temp.getProximo() == null){
                    temp.setProximo(bloco);
                    break;
                }

                temp = temp.getProximo();

            }

        }

        memoriaNaoAlocada -= tamanhoReq;
        memoriaAlocada += tamanhoReq;

        refreshColorBox();
        refreshLabels();

    }

    private void adcionarBlocoLivre(BaseBloco bloco){

        bloco.setProximoBlocoLivre(headerLivres);
        bloco.refreshTextAreaColor(BlocoQuickFit.BASE_COR_LIVRE);
        headerLivres = bloco;

    }

    private void removerBlocoLivre(BaseBloco bloco){

        BaseBloco temp = headerLivres;

        if(temp != null){

            if(headerLivres == bloco){
                headerLivres = bloco.getProximoBlocoLivre();
                return;
            }

            while(temp.getProximoBlocoLivre() != null){

                if(temp.getProximoBlocoLivre() == bloco){
                    temp.setProximoBlocoLivre(temp.getProximoBlocoLivre().getProximoBlocoLivre());
                    break;
                }

                temp = temp.getProximoBlocoLivre();

            }

        }

    }

    private void adcionarBLocoQuickList(BaseBloco bloco){

        bloco.refreshTextAreaColor(getCorBloco(bloco.getTamanhoBloco()));

        for(BaseBloco quick : quickList){

            if(quick.getTamanhoBloco() == bloco.getTamanhoBloco()){
                bloco.setProximoBlocoLivre(quick.getProximo());
                quick.setProximo(bloco);
                break;
            }

        }

    }

    private void removerBlocoQuickList(BaseBloco bloco){

        for(BaseBloco quick : quickList){

            if(quick.getTamanhoBloco() == bloco.getTamanhoBloco()){
                quick.setProximo(quick.getProximo().getProximoBlocoLivre());
                break;
            }

        }

    }

    private String colorGenerator(){

        Color color = new Color((int)(Math.random() * 0x1000000));
        String cor = "#"+Integer.toHexString(color.getRGB()).substring(2);

        return cor;
    }

    private synchronized void refreshColorBox(){

        new Thread(()->{

            if((requisicoes.size() != 0) && (headerQuickFit != null)){

                BaseBloco temp = headerQuickFit;

                while(temp != null){

                    if(isBlocoQuick(temp.getTamanhoBloco())){

                        temp.refreshTextAreaColor(getCorBloco(temp.getTamanhoBloco()));

                    }else{

                        if(temp.getProcessoEmUso() != null){
                            temp.refreshTextAreaColor(BlocoQuickFit.BASE_COR_OCUPADO);
                        }else{
                            temp.refreshTextAreaColor(BlocoQuickFit.BASE_COR_LIVRE);
                        }

                    }

                    temp = temp.getProximo();

                }

            }

        }).start();

    }

    private String getCorBloco(int tamanhoReq){

       for (Requisicao requisicao : requisicoes){

           if (requisicao.getTamanhoRequisicao() == tamanhoReq){

               return "text-area-background: " + requisicao.getCor() + ";";

           }

       }

       return BlocoQuickFit.BASE_COR_LIVRE;

    }


    public void refreshLabels(){

        new Thread(()->{

            memoriaAlocada = 0;
            memoriaEmUso = 0;
            memoriaNaoAlocada = 0;

            BaseBloco temp = headerQuickFit;

            while(temp != null){

                if(temp.getProcessoEmUso() != null){
                    memoriaEmUso += temp.getTamanhoEmUso();
                }

                memoriaAlocada += temp.getTamanhoBloco();

                temp = temp.getProximo();

            }

            memoriaNaoAlocada = memoriaTotal - memoriaAlocada;


            Platform.runLater(() -> {

                labelMemoriaNA.setText("Memória não alocada: " + memoriaNaoAlocada + "KB");
                labelMemoriaAlocada.setText("Memória alocada: " + memoriaAlocada + "KB");
                labelMemoriaEmUso.setText("Memória em uso: " + memoriaEmUso + "KB");

            });

        }).start();

    }

    private void refreshLabelsBlocosQuick(){

        new Thread(()->{

            Platform.runLater(()->{

                labelBlocosQuick.setText("Blocos QuickFit: ");

            });

            for(BaseBloco bloco : quickList){

                Platform.runLater(()->{

                    labelBlocosQuick.setText(labelBlocosQuick.getText() + bloco.getTamanhoBloco() + "; ");

                });

            }

        }).start();

    }

    private class Requisicao implements Comparable<Requisicao>{

        int tamanhoReq;
        int qtdReq;
        String cor;

        public Requisicao(int tamanhoReq, String cor){

            this.tamanhoReq = tamanhoReq;
            this.cor = cor;
            qtdReq = 1;

        }

        public void incRequisicao(){
            this.qtdReq++;
        }

        public int getTamanhoRequisicao(){
            return tamanhoReq;
        }

        public int getQtdRequisicoes() {
            return qtdReq;
        }

        public String getCor() {
            return cor;
        }

        @Override
        public int compareTo(Requisicao proximo) {

            if(this.getQtdRequisicoes() > proximo.getQtdRequisicoes()){
                return -1;
            }else if(this.getQtdRequisicoes() < proximo.getQtdRequisicoes()){
                return 1;
            }else{
                return 0;
            }

        }

    }




}
