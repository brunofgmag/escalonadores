package main;

import controllers.ApplicationController;

public class Main{
    public static final String TITLE = "Escalonadores";

    public static void main(String[] args){
        ApplicationController.getInstance().run();
    }
}