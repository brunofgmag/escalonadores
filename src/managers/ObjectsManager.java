package managers;

import javafx.scene.control.RadioButton;
import models.Button;
import models.Field;
import models.Radio;

import java.util.EnumMap;

public class ObjectsManager {
    private static ObjectsManager instance;
    private final EnumMap<Field, javafx.scene.control.TextField> jFieldENumMap = new EnumMap<>(Field.class);
    private final EnumMap<Button, javafx.scene.control.Button> jButtonEnumMap = new EnumMap<>(Button.class);
    private final EnumMap<Radio, RadioButton> jRadioEnumMap = new EnumMap<>(Radio.class);

    /**
     * Construtor privado para classe singleton
     */
    private ObjectsManager() {
    }

    /**
     * Método de retorno da instância singleton
     *
     * @return instância singleton
     */
    public static ObjectsManager getInstance() {
        if (instance == null) {
            instance = new ObjectsManager();
        }
        return instance;
    }

    public void addField(Field ID, javafx.scene.control.TextField field) {
        System.out.println("Field [" + ID + "] adicionado");
        jFieldENumMap.put(ID, field);
    }

    public void addButton(Button ID, javafx.scene.control.Button button) {
        System.out.println("Button [" + ID + "] adicionado");
        jButtonEnumMap.put(ID, button);
    }

    public void addRadio(Radio ID, RadioButton radioButton)
    {
        System.out.println("Radio [" + ID + "] adicionado");
        jRadioEnumMap.put(ID, radioButton);
    }

//    public void cleanManager() {
//        jButtonEnumMap.clear();
//        jFieldENumMap.clear();
//    }

    public javafx.scene.control.TextField getField(Field ID){
        return jFieldENumMap.get(ID);
    }

    public javafx.scene.control.Button getButton(Button ID){
        return jButtonEnumMap.get(ID);
    }

    public RadioButton getRadio(Radio ID)
    {
        return jRadioEnumMap.get(ID);
    }
}