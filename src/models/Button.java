package models;

public enum Button {
    BUTTON_SJF,
    BUTTON_RR,
    BUTTON_IBS,
    BUTTON_LTG
}
