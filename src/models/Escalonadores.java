package models;

public enum Escalonadores {
    SJF,
    ROUND_ROBIN,
    IBS,
    LTG
}
