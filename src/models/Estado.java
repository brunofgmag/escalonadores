package models;

public enum Estado {
    PRONTO,
    BLOQUEADO,
    EXECUTANDO,
    FINALIZADO,
    ABORTADO,
    EM_ESPERA
}
