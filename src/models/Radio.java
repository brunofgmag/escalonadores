package models;

/**
 * Created by Bruno Magalhães on 24/11/2017.
 * Escalonadores
 */
public enum Radio {
    BEST_FIT_SJF,
    QUICK_FIT_SJF,
    MERGE_FIT_SJF,
    BEST_FIT_RR,
    QUICK_FIT_RR,
    MERGE_FIT_RR,
    BEST_FIT_IBS,
    QUICK_FIT_IBS,
    MERGE_FIT_IBS,
    BEST_FIT_LTG,
    QUICK_FIT_LTG,
    MERGE_FIT_LTG
}
