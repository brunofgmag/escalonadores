package objects;

import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;

/**
 * Created by Bruno on 16/11/2017.
 * Escalonadores
 */
public abstract class BaseBloco {

    private double idBloco;
    private int tamanhoBloco;
    private int tamanhoEmUso;
    private BaseBloco proximo;
    private BaseBloco proximoBlocoLivre;
    private BaseProcesso processoEmUso;
    private HBox blocosMemoriaBox;
    protected TextArea textArea;

    BaseBloco(int tamanhoBloco, BaseProcesso processoEmUso, HBox blocosMemoriaBox)
    {
        this.processoEmUso = processoEmUso;
        this.blocosMemoriaBox = blocosMemoriaBox;
        this.tamanhoBloco = tamanhoBloco;
        this.textArea = new TextArea();
    }

    BaseBloco(double idBloco, int tamanhoBloco, BaseProcesso processoEmUso, HBox blocosMemoriaBox)
    {
        this.idBloco = idBloco;
        this.processoEmUso = processoEmUso;
        this.blocosMemoriaBox = blocosMemoriaBox;
        this.tamanhoBloco = tamanhoBloco;
        this.textArea = new TextArea();
    }

    public void alocarBloco(BaseProcesso processo, int tamanhoRequisicao)
    {
        processoEmUso = processo;
    }

    public abstract void desalocar();

    public BaseBloco getProximoBlocoLivre()
    {
        return proximoBlocoLivre;
    }

    public void setProximoBlocoLivre(BaseBloco proximoBlocoLivre)
    {
        this.proximoBlocoLivre = proximoBlocoLivre;
    }

    public void setTamanhoBloco(int tamanhoBloco) {
        this.tamanhoBloco = tamanhoBloco;
    }

    public int getTamanhoBloco()
    {
        return tamanhoBloco;
    }

    public BaseBloco getProximo() {
        return proximo;
    }

    public void setProximo(BaseBloco proximo) {
        this.proximo = proximo;
    }

    public BaseProcesso getProcessoEmUso() {
        return processoEmUso;
    }

    public void setProcessoEmUso(BaseProcesso processoEmUso) {
        this.processoEmUso = processoEmUso;
    }

    public boolean isEmpty(){

        return processoEmUso == null;

    }

    public HBox getBlocosMemoriaBox() {
        return blocosMemoriaBox;
    }

    public int getTamanhoEmUso() {
        return tamanhoEmUso;
    }

    public void setTamanhoEmUso(int tamanhoEmUso) {
        this.tamanhoEmUso = tamanhoEmUso;
    }

    public double getIdBloco() {
        return idBloco;
    }

    public void setIdBloco(double idBloco) {
        this.idBloco = idBloco;
    }

    public TextArea getTextArea() {
        return textArea;
    }

    public void setTextArea(TextArea textArea) {
        this.textArea = textArea;
    }

    public void refreshTextArea(){}

    public void refreshTextAreaColor(String cor){}
}
