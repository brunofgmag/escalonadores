package objects;

import gerenciadores.BaseGerenciador;
import javafx.scene.control.TextArea;
import models.Estado;

import java.util.concurrent.ThreadLocalRandom;

public abstract class BaseProcesso {
    Estado estado;
    int tempoDeExecucao;
    int idProcesso;
    int tempoRestante;
    int deadline;
    int prioridade;
    int quantum;
    int start;
    int end;
    int idCore;
    int quantidadeMemoria;
    TextArea textArea;
    models.Processo tipoProcesso;
    BaseGerenciador gerenciador;
    // propriedades da fila
    private BaseProcesso processo;

    public void abortar(String motivo){
        this.estado = Estado.ABORTADO;
    }

//    public void bloquear(){
//        this.estado = Estado.BLOQUEADO;
//    }

    void setPronto(){
        this.estado = Estado.PRONTO;
    }

    public boolean executar(){
        this.estado = Estado.EXECUTANDO;

        return true;
    }

    public void finalizar()
    {
        this.estado = Estado.FINALIZADO;
    }

    public Estado getEstado(){
        return estado;
    }

    int getTempoRestante(){
        return tempoRestante;
    }

    public int getTempoDeExecucao(){
        return tempoDeExecucao;
    }

    public int getIdProcesso(){
        return idProcesso;
    }

    public int getDeadline(){
        return deadline;
    }

    int getQuantum(){
        return quantum;
    }

    public int getPrioridade(){
        return prioridade;
    }

    public BaseProcesso getNext(){
        return processo;
    }

    public int getQuantidadeMemoria() {
        return quantidadeMemoria;
    }

    public void setNext(BaseProcesso processo){
        this.processo = processo;
    }

//    public TextArea getTextArea(){
//        return textArea;
//    }

    models.Processo getTipoProcesso(){
        return tipoProcesso;
    }

    public void setQuantum(int quantum){
        this.quantum = quantum;
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

//    public int getIdCore() {
//        return idCore;
//    }

    public void setIdCore(int idCore) {
        this.idCore = idCore;
    }

//    public void setEstado(Estado estado) {
//        this.estado = estado;
//    }

    public boolean alocarMemoria(){

        int novaReqMem = ThreadLocalRandom.current().nextInt(32, 1024 + 1);
        quantidadeMemoria += novaReqMem;

        if(!gerenciador.alocarMemoria(novaReqMem, this)){

            abortar("OOM");

            return false;

        }

        return true;

    }
}
