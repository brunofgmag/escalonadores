package objects;

import javafx.application.Platform;
import javafx.scene.layout.HBox;

/**
 * Created by Bruno on 21/11/2017.
 * Escalonadores
 */
public class BlocoBestFit extends BaseBloco{
    private final int idBloco;

    public BlocoBestFit(int idBloco, int tamanhoBloco, BaseProcesso processo, HBox memoriaBox) {
        super(tamanhoBloco, processo, memoriaBox);

        this.idBloco = idBloco;

        setTamanhoEmUso(tamanhoBloco);

        Platform.runLater(() -> {
            textArea.setPrefRowCount(4);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + tamanhoBloco + "KB" +
                    "\nUSO: " + tamanhoBloco + "KB" +
                    "\nPRO: " + processo.getIdProcesso());
            textArea.setEditable(false);

            if(processo.getTipoProcesso().equals(models.Processo.ESTATICO)){
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }

            memoriaBox.getChildren().add(textArea);
        });
    }

    @Override
    public synchronized void desalocar() {
        setProcessoEmUso(null);

        setTamanhoEmUso(0);

        Platform.runLater(() -> {
            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + "Livre" +
                    "\nPRO: " + "Livre");

            textArea.setStyle("text-area-background: white;");
        });
    }

    @Override
    public void alocarBloco(BaseProcesso processo, int tamanhoRequisicao) {
        super.alocarBloco(processo, tamanhoRequisicao);

        setTamanhoEmUso(tamanhoRequisicao);

        Platform.runLater(() -> {
            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + tamanhoRequisicao + "KB" +
                    "\nPRO: " + processo.getIdProcesso());

            if(processo.getTipoProcesso().equals(models.Processo.ESTATICO)){
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }
        });
    }
}
