package objects;

import javafx.application.Platform;
import javafx.scene.layout.HBox;

import java.text.DecimalFormat;

/**
 * Created by Daniel on 22/11/2017.
 */

public class BlocoMergeFit extends BaseBloco {

    DecimalFormat df = new DecimalFormat("0.000000");

    public BlocoMergeFit(double idBloco, int tamanhoBloco, BaseProcesso processo, HBox memoriaBox, boolean isToAdd) {

        super(idBloco, tamanhoBloco, processo, memoriaBox);

        Platform.runLater(() -> {
            textArea.setPrefRowCount(4);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + df.format(getIdBloco()) +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + "Livre" +
                    "\nPRO: " + "Livre");
            textArea.setEditable(false);

            textArea.setStyle("text-area-background: white;");

            if(isToAdd){
                memoriaBox.getChildren().add(textArea);
            }

        });

    }

    @Override
    public void desalocar() {//era syncronixes

        setProcessoEmUso(null);

        setTamanhoEmUso(0);

        Platform.runLater(() -> {
            textArea.setText("ID: " + df.format(getIdBloco()) +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + "Livre" +
                    "\nPRO: " + "Livre");

            textArea.setStyle("text-area-background: white;");
        });
    }

    @Override
    public void alocarBloco(BaseProcesso processo, int tamanhoRequisicao) {

        super.alocarBloco(processo, tamanhoRequisicao);

        setTamanhoEmUso(tamanhoRequisicao);

        Platform.runLater(() -> {
            textArea.setText("ID: " + df.format(getIdBloco())  +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + tamanhoRequisicao + "KB" +
                    "\nPRO: " + processo.getIdProcesso());

            if(processo.getTipoProcesso().equals(models.Processo.ESTATICO)){
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }
        });
    }

    @Override
    public void refreshTextArea() {
        super.refreshTextArea();

        if(getProcessoEmUso() == null){

            Platform.runLater(() -> {
                textArea.setText("ID: " + df.format(getIdBloco()) +
                        "\nTAM: " + getTamanhoBloco() + "KB" +
                        "\nUSO: " + "Livre" +
                        "\nPRO: " + "Livre");

                textArea.setStyle("text-area-background: white;");
            });

        }else{

            Platform.runLater(() -> {
                textArea.setText("ID: " + df.format(getIdBloco()) +
                        "\nTAM: " + getTamanhoBloco() + "KB" +
                        "\nUSO: " + getTamanhoBloco() + "KB" +
                        "\nPRO: " + getProcessoEmUso().getIdProcesso());

                if(getProcessoEmUso().getTipoProcesso().equals(models.Processo.ESTATICO)){
                    textArea.setStyle("text-area-background: grey;");
                } else {
                    textArea.setStyle("text-area-background: red;");
                }
            });
        }
    }
}
