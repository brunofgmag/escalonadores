package objects;

import javafx.application.Platform;
import javafx.scene.layout.HBox;

public class BlocoQuickFit extends BaseBloco {

    private final int idBloco;
    public static final String BASE_COR_OCUPADO = "text-area-background: grey;";
    public static final String BASE_COR_LIVRE = "text-area-background: white;";

    public BlocoQuickFit(int idBloco, int tamanhoBloco, BaseProcesso processoEmUso, HBox blocosMemoriaBox) {

        super(tamanhoBloco, processoEmUso, blocosMemoriaBox);

        this.idBloco = idBloco;

        setTamanhoEmUso(tamanhoBloco);

        Platform.runLater(() -> {
            textArea.setPrefRowCount(4);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + tamanhoBloco + "KB" +
                    "\nUSO: " + tamanhoBloco + "KB" +
                    "\nPRO: " + processoEmUso.getIdProcesso());

            textArea.setEditable(false);
            textArea.setStyle(BASE_COR_OCUPADO);

            blocosMemoriaBox.getChildren().add(textArea);
        });
    }

    public BlocoQuickFit(int tamanhoBloco){//bloco header da quickList

        super(tamanhoBloco, null, null);
        this.idBloco = -1;

    }

    @Override
    public void alocarBloco(BaseProcesso processo, int tamanhoRequisicao) {

        super.alocarBloco(processo, tamanhoRequisicao);

        setTamanhoEmUso(tamanhoRequisicao);

        Platform.runLater(() -> {

            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + tamanhoRequisicao + "KB" +
                    "\nPRO: " + processo.getIdProcesso());

        });
    }

    @Override
    public void desalocar() {

        setProcessoEmUso(null);

        setTamanhoEmUso(0);

        Platform.runLater(() -> {

            textArea.setText("ID: " + idBloco +
                    "\nTAM: " + getTamanhoBloco() + "KB" +
                    "\nUSO: " + "Livre" +
                    "\nPRO: " + "Livre");

        });

    }

    @Override
    public void refreshTextAreaColor(String cor) {
        super.refreshTextAreaColor(cor);

        Platform.runLater(()->{

            textArea.setStyle(cor);

        });
    }

    @Override
    public void refreshTextArea() {
        super.refreshTextArea();
    }
}
