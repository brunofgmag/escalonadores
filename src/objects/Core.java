package objects;

import escalonadores.*;
import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import models.Estado;

public class Core {
    private final BaseEscalonador escalonador;
    private BaseProcesso currentProcesso;
    private final BaseGerenciador gerenciador;
    private final TextArea textArea;
    private final int numCore;
    private boolean running = true;

    public Core(BaseEscalonador escalonador, BaseGerenciador gerenciador, HBox coresBox, int numCore){

        this.gerenciador = gerenciador;
        this.numCore = numCore;
        this.escalonador = escalonador;
        this.textArea = new TextArea();
        this.textArea.setPrefRowCount(6);
        this.textArea.setPrefColumnCount(7);
        this.textArea.setEditable(false);

        coresBox.getChildren().add(textArea);

        run();
    }

    public BaseProcesso getProcesso(){

        return currentProcesso;
    }

    private void run(){
        new Thread(() -> {
            try{
                while (running){
                    Platform.runLater(() -> {
                        if(currentProcesso == null || !currentProcesso.getEstado().equals(Estado.EXECUTANDO) || (currentProcesso instanceof ProcessoRR && currentProcesso.getQuantum() == 0)){
                            currentProcesso = escalonador.getProcesso(numCore);
                        }

                        if(currentProcesso != null && (currentProcesso.getEstado().equals(Estado.PRONTO) || currentProcesso.getEstado().equals(Estado.EXECUTANDO))){
                            if(currentProcesso.getEstado() != Estado.EXECUTANDO && !currentProcesso.executar())
                            {
                                currentProcesso.abortar("OOM");
                                currentProcesso = null;
                            }
                            else
                            {
                                if(currentProcesso.getTipoProcesso().equals(models.Processo.ESTATICO)){
                                    textArea.setStyle("text-area-background: grey;");
                                } else {
                                    textArea.setStyle("text-area-background: red;");
                                }

                                if(escalonador instanceof SJF){

                                    textArea.setText("ID: " + currentProcesso.getIdProcesso() +
                                            "\nTE: " + currentProcesso.getTempoDeExecucao() +
                                            "\nTR: " + currentProcesso.getTempoRestante() +
                                            "\nMEM: " + currentProcesso.getQuantidadeMemoria() + "KB");

                                }else if(escalonador instanceof RR){

                                    textArea.setText("ID: " + currentProcesso.getIdProcesso() +
                                            "\nTE: " + currentProcesso.getTempoDeExecucao() +
                                            "\nTR: " + currentProcesso.getTempoRestante() +
                                            "\nP: " + currentProcesso.getPrioridade() +
                                            "\nQ: " + currentProcesso.getQuantum() +
                                            "\nMEM: " + currentProcesso.getQuantidadeMemoria() + "KB");

                                }else if(escalonador instanceof IBS){

                                    textArea.setText("ID: " + currentProcesso.getIdProcesso() +
                                            "\nTE: " + currentProcesso.getTempoDeExecucao() +
                                            "\nTR: " + currentProcesso.getTempoRestante() +
                                            "\nS: " + currentProcesso.getStart() +
                                            "\nE: " + currentProcesso.getEnd()  +
                                            "\nMEM: " + currentProcesso.getQuantidadeMemoria() + "KB");

                                }else if(escalonador instanceof LTG){

                                    textArea.setText("ID: " + currentProcesso.getIdProcesso() +
                                            "\nTE: " + currentProcesso.getTempoDeExecucao() +
                                            "\nTR: " + currentProcesso.getTempoRestante() +
                                            "\nD: " + currentProcesso.getDeadline() +
                                            "\nMEM: " + currentProcesso.getQuantidadeMemoria() + "KB");

                                }
                            }
                        } else {
                            textArea.setStyle("text-area-background: white;");

                            textArea.setText("");
                        }
                    });
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
