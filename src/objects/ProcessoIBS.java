package objects;

import escalonadores.IBS;
import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import models.Estado;
import models.Processo;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;

public class ProcessoIBS extends BaseProcesso{

    private final IBS.Time tempoExecutado;
    private final HBox esperaBox;
    private final HBox abortadosBox;
    private final HBox finalizadoBox;
    private HBox aptosBox;
    private boolean firstExec = true;

    public ProcessoIBS(int idProcesso, IBS.Time tempoExecutado, HBox esperaBox, HBox abortadosBox, HBox finalizadoBox, BaseGerenciador gerenciador, models.Processo tipoProcesso){
        this.estado = Estado.BLOQUEADO;

        this.tempoExecutado = tempoExecutado;
        this.esperaBox = esperaBox;
        this.abortadosBox = abortadosBox;
        this.finalizadoBox = finalizadoBox;

        this.gerenciador = gerenciador;

        this.textArea = new TextArea();
        this.idProcesso = idProcesso;
        this.tipoProcesso = tipoProcesso;
        this.idCore = -1;

        int timeSize = 50;

        if(tipoProcesso == Processo.DINAMICO){

            timeSize = tempoExecutado.getTime() + 50;

            this.start = ThreadLocalRandom.current().nextInt(tempoExecutado.getTime() + 1, timeSize);

            this.end = ThreadLocalRandom.current().nextInt(start + 10, start + 31);

        }else{

            this.start = ThreadLocalRandom.current().nextInt(0, timeSize);

            this.end = ThreadLocalRandom.current().nextInt(start + 10, start + 31);

        }

        this.quantidadeMemoria = ThreadLocalRandom.current().nextInt(32, 1024 + 1);
        this.tempoDeExecucao = end - start;
        this.tempoRestante = tempoDeExecucao;


        Platform.runLater(() -> {
            textArea.setPrefRowCount(5);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + idProcesso +
                    "\nS: " + start +
                    "\nE: " + end +
                    "\nMEM: " + quantidadeMemoria + "KB");
            textArea.setEditable(false);

            if (tipoProcesso.equals(Processo.ESTATICO)) {
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }

        });

        run();

    }

    private void run() {

        new Thread(() -> {
            try {
                    boolean callOnce = false;
                    while(!estado.equals(Estado.FINALIZADO)){
                        Thread.sleep(1000);

                        if (estado == Estado.ABORTADO)
                        {
                            break;
                        }

                       if(idCore >= 0){

                           if(estado.equals(Estado.EXECUTANDO)){

                               if(tempoRestante > 1){
                                   if(!callOnce)
                                   {

                                       if(!alocarMemoria()){
                                           break;
                                       }

                                       callOnce = true;
                                   }

                                   if(Math.random() <= 0.25)
                                   {
                                       if(!alocarMemoria()){
                                           break;
                                       }
                                   }

                                   tempoRestante--;
                               }else{
                                   finalizar();

                                   Platform.runLater(() -> {
                                       textArea.setText("ID: " + idProcesso +
                                               "\nS: " + start +
                                               "\nE: " + end +
                                               "\nMEM: " + quantidadeMemoria + "KB");
                                       finalizadoBox.getChildren().add(textArea);
                                   });

                                   return;
                               }

                           }else if(estado.equals(Estado.PRONTO)){

                               Platform.runLater(() -> textArea.setText("ID: " + idProcesso +
                                       "\nS: " + start +
                                       "\nE: " + end +
                                       "\nMEM: " + quantidadeMemoria + "KB"));

                           }

                       }

                        if(tempoExecutado.getTime() > start && estado != Estado.EXECUTANDO) {
                           abortar("Tempo");
                           return;
                        }

                    }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();

    }

    @Override
    public boolean executar(){
        boolean req = true;

        if(firstExec)
        {
            req = gerenciador.alocarMemoria(quantidadeMemoria, this);
            firstExec = false;
        }
        if(req) {
            estado = Estado.EXECUTANDO;
            Platform.runLater(() -> aptosBox.getChildren().remove(textArea));

            return true;
        }

        return false;
    }

    public void setPronto(final HBox aptos) {
        super.setPronto();

        if(tempoExecutado.getTime() > start && estado != Estado.EXECUTANDO) {
            abortar("Tempo");
        }

        Platform.runLater(() -> esperaBox.getChildren().remove(textArea));

        if(this.aptosBox != null){
            Platform.runLater(() -> {
                aptosBox.getChildren().remove(textArea);
                this.aptosBox = aptos;
                aptosBox.getChildren().addAll(textArea);
            });
        }else{
            this.aptosBox = aptos;
            Platform.runLater(() -> aptosBox.getChildren().add(textArea));
        }
    }

    public void setEmEspera(){
        if(estado != Estado.PRONTO && estado != Estado.BLOQUEADO){
            return;
        }

        if(tempoExecutado.getTime() > start && estado != Estado.EXECUTANDO) {
            abortar("Tempo");
        }

        estado = Estado.EM_ESPERA;
        Platform.runLater(() -> {
            if(aptosBox != null){
                aptosBox.getChildren().remove(textArea);
            }
            if(!esperaBox.getChildren().contains(textArea)){
                esperaBox.getChildren().add(textArea);
            }
        });
    }

    @Override
    public void abortar(String motivo){
        super.abortar(motivo);

        abortarGui(motivo);

        // IMPORTANTE! Esse timer garante que o processo irá para a lista de abortados na GUI após o processo de escalonamento terminar
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                abortarGui(motivo);
            }
        }, 1500);

        gerenciador.desalocarMemoria(this);
    }

    private void abortarGui(String motivo){
        Platform.runLater(() -> {
            textArea.setText("ID: " + idProcesso +
                    "\nS: " + start +
                    "\nE: " + end +
                    "\nMEM: " + quantidadeMemoria + "KB" +
                    "\nMOT: " + motivo);
            if(aptosBox != null){
                aptosBox.getChildren().remove(textArea);
            }
            esperaBox.getChildren().remove(textArea);
            if(!abortadosBox.getChildren().contains(textArea)){
                abortadosBox.getChildren().add(textArea);
            }
        });
    }

    @Override
    public void finalizar() {
        super.finalizar();

        gerenciador.desalocarMemoria(this);
    }
}
