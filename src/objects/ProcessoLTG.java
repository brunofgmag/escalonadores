package objects;

import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import models.Estado;
import models.Processo;

import java.util.concurrent.ThreadLocalRandom;

public class ProcessoLTG extends BaseProcesso {
    private final HBox aptosBox;
    private final HBox finalizadosBox;
    private final HBox abortadosBox;

    public ProcessoLTG(int idProcesso, HBox aptosBox, HBox finalizadosBox, HBox bloqueadosBox, BaseGerenciador gerenciador, models.Processo tipoProcesso){
        estado = Estado.PRONTO;
        tempoDeExecucao = ThreadLocalRandom.current().nextInt(10, 30 + 1);
        quantidadeMemoria = ThreadLocalRandom.current().nextInt(32, 1024 + 1);
        deadline = ThreadLocalRandom.current().nextInt(4, 20 + 1);
        tempoRestante = tempoDeExecucao;

        this.gerenciador = gerenciador;
        this.textArea = new TextArea();
        this.aptosBox = aptosBox;
        this.finalizadosBox = finalizadosBox;
        this.idProcesso = idProcesso;
        this.tipoProcesso = tipoProcesso;
        this.abortadosBox = bloqueadosBox;

        Platform.runLater(() -> {
            textArea.setPrefRowCount(5);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + idProcesso +
                    "\nTR: " + tempoRestante +
                    "\nD: " + deadline +
                    "\nMEM: " + quantidadeMemoria + "KB");
            textArea.setEditable(false);

            if (tipoProcesso.equals(Processo.ESTATICO)) {
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }

            aptosBox.getChildren().add(textArea);
        });

        run();
    }

    private void run(){
        new Thread(() -> {
            try {
                boolean callOnce = false;
                while (!estado.equals(Estado.FINALIZADO)){
                    Thread.sleep(1000);

                    if (estado == Estado.ABORTADO)
                    {
                        break;
                    }

                    if(deadline > 0 && estado.equals(Estado.PRONTO)){
                        deadline--;

                        Platform.runLater(() -> textArea.setText("ID: " + idProcesso +
                                "\nTR: " + tempoRestante +
                                "\nD: " + deadline +
                                "\nMEM: " + quantidadeMemoria + "KB"));
                    }else if (deadline <= 0 && !estado.equals(Estado.EXECUTANDO)){
                        abortar("Deadline");

                        return;
                    }

                    if(estado.equals(Estado.EXECUTANDO) && tempoRestante > 0){
                        if(!callOnce)
                        {
                            alocarMemoria();
                            callOnce = true;
                        }

                        if(Math.random() <= 0.25)
                        {
                            alocarMemoria();
                        }

                        tempoRestante--;

                        if (tempoRestante <= 0){
                            finalizar();

                            Platform.runLater(() -> {
                                textArea.setText("ID: " + idProcesso +
                                        "\nTE: " + tempoDeExecucao +
                                        "\nD: " + deadline +
                                        "\nMEM: " + quantidadeMemoria + "KB");
                                finalizadosBox.getChildren().add(textArea);
                            });
                        }
                    }
                }

                estado = Estado.FINALIZADO;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public boolean executar(){
        if(gerenciador.alocarMemoria(quantidadeMemoria, this))
        {
            estado = Estado.EXECUTANDO;
            Platform.runLater(() -> aptosBox.getChildren().remove(textArea));

            return true;
        }

        return false;
    }

    @Override
    public void finalizar() {
        super.finalizar();

        gerenciador.desalocarMemoria(this);
    }

    @Override
    public void abortar(String motivo) {
        estado = Estado.ABORTADO;

        gerenciador.desalocarMemoria(this);

        Platform.runLater(() -> {
            textArea.setText("ID: " + idProcesso +
                    "\nTE: " + tempoDeExecucao +
                    "\nMEM: " + quantidadeMemoria + "KB" +
                    "\nMOT: " + motivo);
            abortadosBox.getChildren().addAll(textArea);
        });
    }
}
