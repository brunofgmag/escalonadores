package objects;

import escalonadores.RR;
import gerenciadores.BaseGerenciador;
import javafx.application.Platform;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import models.Estado;
import models.Processo;

import java.util.concurrent.ThreadLocalRandom;

public class ProcessoRR extends BaseProcesso {
    private final HBox finalizadosBox;
    private final HBox abortadosBox;
    private final HBox p0box;
    private final HBox p1box;
    private final HBox p2box;
    private final HBox p3box;
    private final RR escalonadorRR;
    private boolean firstExec = true;

    public ProcessoRR(int idProcesso, HBox p0box, HBox p1box, HBox p2box, HBox p3box, HBox finalizadosBox, HBox abortadosBox, BaseGerenciador gerenciador, RR escalonadorRR, models.Processo tipoProcesso){
        estado = Estado.PRONTO;

        this.idProcesso = idProcesso;
        this.p0box = p0box;
        this.p1box = p1box;
        this.p2box = p2box;
        this.p3box = p3box;
        this.gerenciador = gerenciador;
        this.finalizadosBox = finalizadosBox;
        this.abortadosBox = abortadosBox;
        this.escalonadorRR = escalonadorRR;
        this.tipoProcesso = tipoProcesso;
        this.textArea = new TextArea();

        tempoDeExecucao = ThreadLocalRandom.current().nextInt(10, 30 + 1);
        prioridade = ThreadLocalRandom.current().nextInt(0, 3 + 1);
        quantidadeMemoria = ThreadLocalRandom.current().nextInt(32, 1024 + 1);
        tempoRestante = tempoDeExecucao;

        Platform.runLater(() -> {
            textArea.setPrefRowCount(5);
            textArea.setPrefColumnCount(7);
            textArea.setText("ID: " + idProcesso +
                    "\nTR: " + tempoRestante +
                    "\nP: " + prioridade +
                    "\nMEM: " + quantidadeMemoria + "KB");
            textArea.setEditable(false);

            if (tipoProcesso.equals(Processo.ESTATICO)) {
                textArea.setStyle("text-area-background: grey;");
            } else {
                textArea.setStyle("text-area-background: red;");
            }

            switch (prioridade){
                case 0: p0box.getChildren().add(textArea);
                    break;
                case 1: p1box.getChildren().add(textArea);
                    break;
                case 2: p2box.getChildren().add(textArea);
                    break;
                case 3: p3box.getChildren().add(textArea);
            }
        });

        run();
    }

    private void run(){
        new Thread(() -> {
            try {
                boolean callOnce = false;
                while (!estado.equals(Estado.FINALIZADO)){
                    Thread.sleep(1000);

                    if (estado == Estado.ABORTADO)
                    {
                        break;
                    }

                    if(estado.equals(Estado.EXECUTANDO) && tempoRestante > 0) {

                        if (quantum > 0) {

                            if(!callOnce){

                                if(!alocarMemoria()){
                                    break;
                                }

                                callOnce = true;

                            }else if(Math.random() <= 0.25) {

                                if(!alocarMemoria()){
                                    break;
                                }

                            }

                            tempoRestante--;
                            quantum--;

                            if (quantum <= 0) {
                                Platform.runLater(() -> {
                                    textArea.setText("ID: " + idProcesso +
                                            "\nTR: " + tempoRestante +
                                            "\nP: " + prioridade +
                                            "\nMEM: " + quantidadeMemoria + "KB");

                                    switch (prioridade) {
                                        case 0:
                                            p0box.getChildren().add(textArea);
                                            break;
                                        case 1:
                                            p1box.getChildren().add(textArea);
                                            break;
                                        case 2:
                                            p2box.getChildren().add(textArea);
                                            break;
                                        case 3:
                                            p3box.getChildren().add(textArea);
                                    }
                                });
                            }
                        } else {
                            estado = Estado.PRONTO;
                            escalonadorRR.putBack(this);
                        }

                        if (tempoRestante <= 0) {
                            finalizar();

                            Platform.runLater(() -> {
                                textArea.setText("ID: " + idProcesso +
                                        "\nTE: " + tempoDeExecucao +
                                        "\nP: " + prioridade +
                                        "\nMEM: " + quantidadeMemoria + "KB");

                                finalizadosBox.getChildren().add(textArea);

                            });

                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public boolean executar(){
        boolean req = true;

        if(firstExec)
        {
            req = gerenciador.alocarMemoria(quantidadeMemoria, this);
            firstExec = false;
        }
        if(req)
        {
            estado = Estado.EXECUTANDO;

            Platform.runLater(() -> {
                switch (prioridade){
                    case 0: p0box.getChildren().remove(textArea);
                        break;
                    case 1: p1box.getChildren().remove(textArea);
                        break;
                    case 2: p2box.getChildren().remove(textArea);
                        break;
                    case 3: p3box.getChildren().remove(textArea);
                }
            });

            return true;
        }

        return false;
    }

    @Override
    public void finalizar() {

        super.finalizar();

        gerenciador.desalocarMemoria(this);

        Platform.runLater(() -> {
            switch (prioridade){
                case 0: p0box.getChildren().remove(textArea);
                    break;
                case 1: p1box.getChildren().remove(textArea);
                    break;
                case 2: p2box.getChildren().remove(textArea);
                    break;
                case 3: p3box.getChildren().remove(textArea);
            }
        });
    }

    @Override
    public void abortar(String motivo) {

        estado = Estado.ABORTADO;

        gerenciador.desalocarMemoria(this);

        Platform.runLater(() -> {

            switch (prioridade){
                case 0: p0box.getChildren().remove(textArea);
                    break;
                case 1: p1box.getChildren().remove(textArea);
                    break;
                case 2: p2box.getChildren().remove(textArea);
                    break;
                case 3: p3box.getChildren().remove(textArea);
            }

            textArea.setText("ID: " + idProcesso +
                    "\nTE: " + tempoDeExecucao +
                    "\nP: " + prioridade +
                    "\nMEM: " + quantidadeMemoria + "KB" +
                    "\nMOT: " + motivo);

            abortadosBox.getChildren().add(textArea);

        });

    }
}
