package views;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managers.ObjectsManager;
import models.Button;
import models.Escalonadores;
import models.Field;
import models.Radio;

class BaseBox {
    static HBox build(Escalonadores escalonador){
        HBox mainBox = new HBox();

        // Inicio da box de cores

        VBox boxCores = new VBox();

        Label coresLabel = new Label("Número de cores:");
        VBox.setMargin(coresLabel, new Insets(10, 0, 0, 10));

        TextField coresField = new TextField();
        coresField.setEditable(false);
        coresField.setText("1");
        coresField.setPrefColumnCount(1);
        VBox.setMargin(coresField, new Insets(10, 0, 0, 10));

        Slider coresSLider = new Slider();
        coresSLider.setMin(1);
        coresSLider.setMax(64);
        coresSLider.setShowTickLabels(true);
        coresSLider.setShowTickMarks(true);
        coresSLider.setBlockIncrement(1);
        coresSLider.valueProperty().addListener((observable, oldValue, newValue) -> coresField.setText(Integer.toString(newValue.intValue())));
        VBox.setMargin(coresSLider, new Insets(10, 0, 0, 10));

        boxCores.getChildren().addAll(coresLabel, coresField, coresSLider);

        // Fim da box de cores

        // Inicio do box do quantum

        VBox boxQuantum = new VBox();

        Label quantumLabel = new Label("Quantum:");
        VBox.setMargin(quantumLabel, new Insets(10, 0, 0, 10));

        TextField quantumField = new TextField();
        quantumField.setEditable(false);
        quantumField.setText("2");
        quantumField.setPrefColumnCount(1);
        VBox.setMargin(quantumField, new Insets(10, 0, 0, 10));


        Slider quantumSlider = new Slider();
        quantumSlider.setMin(2);
        quantumSlider.setMax(20);
        quantumSlider.setShowTickLabels(true);
        quantumSlider.setShowTickMarks(true);
        quantumSlider.setBlockIncrement(1);
        quantumSlider.valueProperty().addListener((observable, oldValue, newValue) -> quantumField.setText(Integer.toString(newValue.intValue())));
        VBox.setMargin(quantumSlider, new Insets(10, 0, 0, 10));

        boxQuantum.getChildren().addAll(quantumLabel, quantumField, quantumSlider);
        // Fim do box do quantum

        // Inicio do box de procecssos

        VBox boxProcessos = new VBox();

        Label processosLabel = new Label("Quantidade de processos:");
        VBox.setMargin(processosLabel, new Insets(10, 0, 0, 10));

        TextField processosField = new TextField();
        processosField.setText("1");
        processosField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                processosField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        VBox.setMargin(processosField, new Insets(10, 0, 0, 10));

        boxProcessos.getChildren().addAll(processosLabel, processosField);

        // Fim do box de processos

        // Inicio do box de memoria

        VBox boxQtdeMemoria = new VBox();

        Label qtdeMemoriaLabel = new Label("Quantidade de memória (KB):");
        VBox.setMargin(qtdeMemoriaLabel, new Insets(10, 0, 0, 10));

        TextField qtdeMemoriaField = new TextField();
        qtdeMemoriaField.setText("1");
        qtdeMemoriaField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                qtdeMemoriaField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        VBox.setMargin(qtdeMemoriaField, new Insets(10, 0, 0, 10));

        boxQtdeMemoria.getChildren().addAll(qtdeMemoriaLabel, qtdeMemoriaField);

        // Fim do box de memoria

        // Inicio do box filas quickfit

        VBox boxFilasQuickFit = new VBox();

        Label filasQuickFitLabel = new Label("Filas Quick Fit:");
        VBox.setMargin(filasQuickFitLabel, new Insets(10, 0, 0, 10));

        TextField filasQuickFitField = new TextField();
        filasQuickFitField.setText("1");
        filasQuickFitField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                filasQuickFitField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        VBox.setMargin(filasQuickFitField, new Insets(10, 0, 0, 10));

        boxFilasQuickFit.getChildren().addAll(filasQuickFitLabel, filasQuickFitField);
        boxFilasQuickFit.managedProperty().bind(boxFilasQuickFit.visibleProperty());
        boxFilasQuickFit.setVisible(false);


        // Fim do box filas quickfit

        // Inicio do box quantidade processos quickfit

        VBox boxQtdeProcessosQuick = new VBox();

        Label qtdeProcessosQuickLabel = new Label("Qtde. processos Quick Fit:");
        VBox.setMargin(qtdeProcessosQuickLabel, new Insets(10, 0, 0, 10));

        TextField qtdeProcessosQuickField = new TextField();
        qtdeProcessosQuickField.setText("1");
        qtdeProcessosQuickField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                qtdeProcessosQuickField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
        VBox.setMargin(qtdeProcessosQuickField, new Insets(10, 0, 0, 10));

        boxQtdeProcessosQuick.getChildren().addAll(qtdeProcessosQuickLabel, qtdeProcessosQuickField);
        boxQtdeProcessosQuick.managedProperty().bind(boxQtdeProcessosQuick.visibleProperty());
        boxQtdeProcessosQuick.setVisible(false);

        // Fim do box quantidade processos quickfit

        // Inicio do box de gerenciador

        VBox boxGerenciador = new VBox();
        final ToggleGroup group = new ToggleGroup();

        Label gerenciadorLabel = new Label("Gerenciador de Memoria:");
        VBox.setMargin(gerenciadorLabel, new Insets(10, 0, 0, 10));

        RadioButton bestFit = new RadioButton();
        bestFit.setText("Best Fit");
        bestFit.setSelected(true);
        bestFit.setToggleGroup(group);
        VBox.setMargin(bestFit, new Insets(10, 0, 0, 10));

        RadioButton quickFit = new RadioButton();
        quickFit.setText("Quick Fit");
        quickFit.setToggleGroup(group);
        quickFit.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue)
            {
                boxFilasQuickFit.setVisible(true);
                boxQtdeProcessosQuick.setVisible(true);
            }
            else
            {
                boxFilasQuickFit.setVisible(false);
                boxQtdeProcessosQuick.setVisible(false);
            }
        });
        VBox.setMargin(quickFit, new Insets(10, 0, 0, 10));

        RadioButton mergeFit = new RadioButton();
        mergeFit.setText("Merge Fit");
        mergeFit.setToggleGroup(group);
        VBox.setMargin(mergeFit, new Insets(10, 0, 0, 10));

        boxGerenciador.getChildren().addAll(gerenciadorLabel, bestFit, quickFit, mergeFit);

        // Fim do box de gerenciador

        Field cores = Field.CORES_VALUE_SJF;
        Field processos = Field.FIELD_PROCESSOS_SJF;
        Field memoria = Field.FIELD_QTDE_MEMORIA_SJF;
        Field filas = Field.FIELD_FILA_QUICK_SJF;
        Field processosQuick = Field.FIELD_PROCESSOS_QUICK_SJF;
        Radio bestFitRadio = Radio.BEST_FIT_SJF;
        Radio quickFitRadio = Radio.QUICK_FIT_SJF;
        Radio mergeFitRadio = Radio.MERGE_FIT_SJF;
        Button button = Button.BUTTON_SJF;

        javafx.scene.control.Button buttonIniciar = new javafx.scene.control.Button("Iniciar");
        HBox.setMargin(buttonIniciar, new Insets(50, 0, 0, 30));

        // Preenchendo o manager
        switch (escalonador){
            case SJF: cores = Field.CORES_VALUE_SJF;
            button = Button.BUTTON_SJF;
            processos = Field.FIELD_PROCESSOS_SJF;
            bestFitRadio = Radio.BEST_FIT_SJF;
            quickFitRadio = Radio.QUICK_FIT_SJF;
            mergeFitRadio = Radio.MERGE_FIT_SJF;
            memoria = Field.FIELD_QTDE_MEMORIA_SJF;
            filas = Field.FIELD_FILA_QUICK_SJF;
            processosQuick = Field.FIELD_PROCESSOS_QUICK_SJF;
            break;

            case ROUND_ROBIN: cores = Field.CORES_VALUE__RR;
            button = Button.BUTTON_RR;
            processos = Field.FIELD_PROCESSOS_RR;
            bestFitRadio = Radio.BEST_FIT_RR;
            quickFitRadio = Radio.QUICK_FIT_RR;
            mergeFitRadio = Radio.MERGE_FIT_RR;
            memoria = Field.FIELD_QTDE_MEMORIA_RR;
            filas = Field.FIELD_FILA_QUICK_RR;
            processosQuick = Field.FIELD_PROCESSOS_QUICK_RR;
            break;

            case IBS: cores = Field.CORES_VALUE_IBS;
            button = Button.BUTTON_IBS;
            processos = Field.FIELD_PROCESSOS_IBS;
            bestFitRadio = Radio.BEST_FIT_IBS;
            quickFitRadio = Radio.QUICK_FIT_IBS;
            mergeFitRadio = Radio.MERGE_FIT_IBS;
            memoria = Field.FIELD_QTDE_MEMORIA_IBS;
            filas = Field.FIELD_FILA_QUICK_IBS;
            processosQuick = Field.FIELD_PROCESSOS_QUICK_IBS;
            break;

            case LTG: cores = Field.CORES_VALUE_LTG;
            button = Button.BUTTON_LTG;
            processos = Field.FIELD_PROCESSOS_LTG;
            bestFitRadio = Radio.BEST_FIT_LTG;
            quickFitRadio = Radio.QUICK_FIT_LTG;
            mergeFitRadio = Radio.MERGE_FIT_LTG;
            memoria = Field.FIELD_QTDE_MEMORIA_LTG;
            filas = Field.FIELD_FILA_QUICK_LTG;
            processosQuick = Field.FIELD_PROCESSOS_QUICK_LTG;
            break;
        }

        ObjectsManager.getInstance().addField(cores, coresField);
        ObjectsManager.getInstance().addField(processos, processosField);
        ObjectsManager.getInstance().addField(memoria, qtdeMemoriaField);
        ObjectsManager.getInstance().addField(filas, filasQuickFitField);
        ObjectsManager.getInstance().addField(processosQuick, qtdeProcessosQuickField);
        ObjectsManager.getInstance().addRadio(bestFitRadio, bestFit);
        ObjectsManager.getInstance().addRadio(quickFitRadio, quickFit);
        ObjectsManager.getInstance().addRadio(mergeFitRadio, mergeFit);
        ObjectsManager.getInstance().addButton(button, buttonIniciar);

        if(escalonador.equals(Escalonadores.ROUND_ROBIN)){
            ObjectsManager.getInstance().addField(Field.QUANTUM_VALUE_RR, quantumField);
        }

        if(escalonador.equals(Escalonadores.ROUND_ROBIN)){
            mainBox.getChildren().addAll(boxCores, boxQuantum, boxProcessos, boxQtdeMemoria, boxFilasQuickFit, boxQtdeProcessosQuick, boxGerenciador, buttonIniciar);
        }else{
            mainBox.getChildren().addAll(boxCores, boxProcessos, boxQtdeMemoria, boxFilasQuickFit, boxQtdeProcessosQuick, boxGerenciador, buttonIniciar);
        }

        return mainBox;
    }
}
