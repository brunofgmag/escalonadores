package views;

import controllers.ApplicationController;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.TabPane;
import javafx.stage.Screen;
import javafx.stage.Stage;
import main.Main;

public class MainView extends ApplicationController {
    private static MainView instance; // instância singleton
    private Stage stage;
    private boolean started = false;

    /**
     * Classe singleton deve possuir construtor privado
     */
    private MainView() {}

    /**
     * Método responsável pela obtenção da instância singleton
     * @return instância singleton
     */
    public static MainView getInstance(){
        if(instance == null){
            instance = new MainView();
        }

        return instance;
    }

    /**
     * Método responsãvel pela inicialização da interface gráfica
     * @param primaryStage palco de desenho
     * @throws Exception caso ocorra um erro
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        super.start(primaryStage);

        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();

        stage = primaryStage;
        stage.setWidth(primaryScreenBounds.getWidth() - primaryScreenBounds.getWidth()/15);
        stage.setHeight(primaryScreenBounds.getHeight() - primaryScreenBounds.getHeight()/15);
        stage.setX(primaryScreenBounds.getMinX());
        stage.setY(primaryScreenBounds.getMinY());
        stage.setResizable(false);
        stage.setTitle(Main.TITLE);

        Scene scene = new Scene(createPanes());
        scene.getStylesheets().add(ClassLoader.getSystemResource("default.css").toExternalForm());

        stage.setScene(scene);
        stage.show();

        started = true;
    }

    private TabPane createPanes() {
        TabPane panel = new TabPane();
        panel.setMinWidth(stage.getWidth());
        panel.getTabs().addAll(TabOne.build(), TabTwo.build(), TabThree.build(), TabFour.build());

        return panel;
    }

    public boolean hasStarted(){
        return started;
    }
}
