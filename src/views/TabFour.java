package views;

import cpus.CPU;
import escalonadores.LTG;
import gerenciadores.BaseGerenciador;
import gerenciadores.BestFit;
import gerenciadores.MergeFit;
import gerenciadores.QuickFit;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managers.ObjectsManager;
import models.Escalonadores;
import models.Field;
import models.Radio;

class TabFour {
    static Tab build(){
        Tab tab = new Tab();
        tab.setText("LTG");
        tab.setContent(buildBox());
        tab.setClosable(false);
        return tab;
    }

    private static Node buildBox(){
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefWidth(1366);

        VBox vBox = new VBox();

        Label labelMemoria = new Label("Memória:");
        VBox.setMargin(labelMemoria, new Insets(10, 0, 0, 10));

        HBox memoriaBox = new HBox();
        VBox.setMargin(memoriaBox, new Insets(10, 10, 0, 10));

        Label labelBlocosQuick = new Label("");
        VBox.setMargin(labelBlocosQuick, new Insets(10, 0, 0, 10));

        Label labelMemoriaNA = new Label("Memória não alocada: 0KB");
        VBox.setMargin(labelMemoriaNA, new Insets(10, 0, 0, 10));

        Label labelMemoriaAlocada = new Label("Memória alocada: 0KB");
        VBox.setMargin(labelMemoriaAlocada, new Insets(10, 0, 0, 10));

        Label labelMemoriaEmUso = new Label("Memória em uso: 0KB");
        VBox.setMargin(labelMemoriaEmUso, new Insets(10, 0, 0, 10));

        Label labelCores = new Label("Em execução:");
        VBox.setMargin(labelCores, new Insets(10, 0, 0, 10));

        HBox coresBox = new HBox();
        VBox.setMargin(coresBox, new Insets(10, 10, 0, 10));

        Label labelAptos = new Label("Aptos:");
        VBox.setMargin(labelAptos, new Insets(10, 0, 0, 10));

        HBox aptosBox = new HBox();
        VBox.setMargin(aptosBox, new Insets(10, 10, 0, 10));

        Label labelFinalizados = new Label("Finalizados:");
        VBox.setMargin(labelFinalizados, new Insets(10, 0, 0, 10));

        HBox finalizadosBox = new HBox();
        VBox.setMargin(finalizadosBox, new Insets(10, 10, 0, 10));

        Label labelAbortados = new Label("Abortados:");
        VBox.setMargin(labelAbortados, new Insets(10, 0, 0, 10));

        HBox abortadosBox = new HBox();
        VBox.setMargin(abortadosBox, new Insets(10, 10, 0, 10));

        LTG escalonadorLTG = new LTG(aptosBox, finalizadosBox, abortadosBox);

        Button adicionarProcesso = new Button("Adicionar Processo");
        adicionarProcesso.setOnAction(event -> {
            escalonadorLTG.addProcessoDinamico();
            escalonadorLTG.sortViewBox();
        });
        adicionarProcesso.setDisable(true);

        VBox.setMargin(adicionarProcesso, new Insets(10, 0, 0, 10));

        vBox.getChildren().addAll(BaseBox.build(Escalonadores.LTG), adicionarProcesso, labelMemoria, memoriaBox, labelBlocosQuick, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso, labelCores, coresBox, labelAptos, aptosBox, labelFinalizados, finalizadosBox, labelAbortados, abortadosBox);

        scrollPane.setContent(vBox);

        ObjectsManager.getInstance().getButton(models.Button.BUTTON_LTG).setOnAction(event -> {
            int numCores = Integer.valueOf(ObjectsManager.getInstance().getField(Field.CORES_VALUE_LTG).getText());
            int numProcessosIniciais = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_LTG).getText());
            int qtdeMemoria = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_QTDE_MEMORIA_LTG).getText());
            int filasQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_FILA_QUICK_LTG).getText());
            int processosQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_QUICK_LTG).getText());

            BaseGerenciador gerenciador = null;

            if(ObjectsManager.getInstance().getRadio(Radio.BEST_FIT_LTG).isSelected())
            {
                gerenciador = new BestFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.QUICK_FIT_LTG).isSelected())
            {
                gerenciador = new QuickFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso,labelBlocosQuick, processosQuick, filasQuick);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.MERGE_FIT_LTG).isSelected())
            {
                gerenciador = new MergeFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);

            }

            escalonadorLTG.setGerenciador(gerenciador);

            for(int i = 0; i < numProcessosIniciais; i++){
                escalonadorLTG.addProcessoEstatico();
            }

            escalonadorLTG.sortViewBox();

            new CPU(numCores, escalonadorLTG, gerenciador, coresBox);

            ObjectsManager.getInstance().getButton(models.Button.BUTTON_LTG).setDisable(true);

            adicionarProcesso.setDisable(false);
        });

        return scrollPane;
    }
}
