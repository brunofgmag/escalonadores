package views;

import cpus.CPU;
import escalonadores.IBS;
import gerenciadores.BaseGerenciador;
import gerenciadores.BestFit;
import gerenciadores.MergeFit;
import gerenciadores.QuickFit;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managers.ObjectsManager;
import models.Button;
import models.Escalonadores;
import models.Field;
import models.Radio;

class TabThree {

    private static IBS escalonadorIBS;
    private static VBox vBox = new VBox();
    private static ScrollPane scrollPane = new ScrollPane();


    static Tab build(){
        Tab tab = new Tab();
        tab.setText("IBS");
        tab.setContent(buildBox());
        tab.setClosable(false);
        return tab;
    }

    private static Node buildBox() {

        scrollPane.setPrefWidth(1366);

        javafx.scene.control.Button adicionarProcesso = new javafx.scene.control.Button("Adicionar Processo");
        adicionarProcesso.setOnAction(event -> escalonadorIBS.addProcessoDinamico());
        adicionarProcesso.setDisable(true);

        VBox.setMargin(adicionarProcesso, new Insets(10, 0, 0, 10));

        Label labelMemoria = new Label("Memória:");
        VBox.setMargin(labelMemoria, new Insets(10, 0, 0, 10));

        HBox memoriaBox = new HBox();
        VBox.setMargin(memoriaBox, new Insets(10, 10, 0, 10));

        Label labelBlocosQuick = new Label("");
        VBox.setMargin(labelBlocosQuick, new Insets(10, 0, 0, 10));

        Label labelMemoriaNA = new Label("Memória não alocada: 0KB");
        VBox.setMargin(labelMemoriaNA, new Insets(10, 0, 0, 10));

        Label labelMemoriaAlocada = new Label("Memória alocada: 0KB");
        VBox.setMargin(labelMemoriaAlocada, new Insets(10, 0, 0, 10));

        Label labelMemoriaEmUso = new Label("Memória em uso: 0KB");
        VBox.setMargin(labelMemoriaEmUso, new Insets(10, 0, 0, 10));

        Label labelTime = new Label("Tempo de execução:");
        VBox.setMargin(labelTime, new Insets(10, 0, 0, 10));

        Label labelCores = new Label("Em execução:");
        VBox.setMargin(labelCores, new Insets(10, 0, 0, 10));

        HBox coresBox = new HBox();
        VBox.setMargin(coresBox, new Insets(10, 10, 0, 10));

        vBox.getChildren().addAll(BaseBox.build(Escalonadores.IBS),adicionarProcesso, labelMemoria, memoriaBox, labelBlocosQuick, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso, labelTime, labelCores, coresBox);
        scrollPane.setContent(vBox);

        ObjectsManager.getInstance().getButton(Button.BUTTON_IBS).setOnAction(event -> {
            int numCores = Integer.valueOf(ObjectsManager.getInstance().getField(Field.CORES_VALUE_IBS).getText());
            int numProcessosIniciais = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_IBS).getText());
            int qtdeMemoria = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_QTDE_MEMORIA_IBS).getText());
            int filasQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_FILA_QUICK_IBS).getText());
            int processosQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_QUICK_IBS).getText());

            inicializa(numCores, labelTime);

            escalonadorIBS.setNumCores(numCores);

            BaseGerenciador gerenciador = null;

            if(ObjectsManager.getInstance().getRadio(Radio.BEST_FIT_IBS).isSelected())
            {
                gerenciador = new BestFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.QUICK_FIT_IBS).isSelected())
            {
                gerenciador = new QuickFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso,labelBlocosQuick, processosQuick, filasQuick);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.MERGE_FIT_IBS).isSelected())
            {
                gerenciador = new MergeFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);

            }

            escalonadorIBS.setGerenciador(gerenciador);

            for(int i = 0;i < numProcessosIniciais; i++){

                escalonadorIBS.addProcessoEstatico();

            }

            CPU cpu = new CPU(numCores, escalonadorIBS, gerenciador, coresBox);
            escalonadorIBS.setCpu(cpu);
            escalonadorIBS.escalonar();
            escalonadorIBS.startTimer();

            ObjectsManager.getInstance().getButton(models.Button.BUTTON_IBS).setDisable(true);

            adicionarProcesso.setDisable(false);

        });

        return scrollPane;

    }

    private static void inicializa(int numCores, Label labelTime){

        HBox[] listaCoresBox = new HBox[numCores];

        for( int i = 0; i < numCores; i++){

            Label labelCore = new Label("Fila Core " + i + ":");
            VBox.setMargin(labelCore, new Insets(10, 0, 0, 10));

            HBox coreBox = new HBox();
            VBox.setMargin(coreBox, new Insets(10, 10, 0, 10));

            listaCoresBox[i] = coreBox;

            vBox.getChildren().addAll(labelCore,coreBox);


        }

        Label labelPronto = new Label("Em espera:");
        VBox.setMargin(labelPronto, new Insets(10,0,0,10));

        HBox prontoBox = new HBox();
        VBox.setMargin(prontoBox, new Insets(10,10,0,10));


        Label labelBloqueados = new Label("Abortados:");
        VBox.setMargin(labelBloqueados, new Insets(10,0,0,10));

        HBox bloqueadosBox = new HBox();
        VBox.setMargin(bloqueadosBox, new Insets(10,10,0,10));

        Label labelFinalizados = new Label("Finalizados:");
        VBox.setMargin(labelFinalizados, new Insets(10, 0, 0, 10));

        HBox finalizadosBox = new HBox();
        VBox.setMargin(finalizadosBox, new Insets(10, 10, 0, 10));


        vBox.getChildren().addAll(labelPronto,prontoBox, labelBloqueados, bloqueadosBox, labelFinalizados, finalizadosBox);

        escalonadorIBS = new IBS(numCores,labelTime, listaCoresBox, prontoBox, bloqueadosBox, finalizadosBox);

        scrollPane.setContent(vBox);


    }
}
