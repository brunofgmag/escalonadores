package views;

import cpus.CPU;
import escalonadores.RR;
import gerenciadores.BaseGerenciador;
import gerenciadores.BestFit;
import gerenciadores.MergeFit;
import gerenciadores.QuickFit;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managers.ObjectsManager;
import models.Escalonadores;
import models.Field;
import models.Radio;

class TabTwo {
    static Tab build(){
        Tab tab = new Tab();
        tab.setText("Round Robin");
        tab.setContent(buildBox());
        tab.setClosable(false);
        return tab;
    }

    private static Node buildBox(){
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefWidth(1366);

        VBox vBox = new VBox();

        Label labelMemoria = new Label("Memória:");
        VBox.setMargin(labelMemoria, new Insets(10, 0, 0, 10));

        Label labelBlocosQuick = new Label("");
        VBox.setMargin(labelBlocosQuick, new Insets(10, 0, 0, 10));

        HBox memoriaBox = new HBox();
        VBox.setMargin(memoriaBox, new Insets(10, 10, 0, 10));

        Label labelMemoriaNA = new Label("Memória não alocada: 0KB");
        VBox.setMargin(labelMemoriaNA, new Insets(10, 0, 0, 10));

        Label labelMemoriaAlocada = new Label("Memória alocada: 0KB");
        VBox.setMargin(labelMemoriaAlocada, new Insets(10, 0, 0, 10));

        Label labelMemoriaEmUso = new Label("Memória em uso: 0KB");
        VBox.setMargin(labelMemoriaEmUso, new Insets(10, 0, 0, 10));

        Label labelCores = new Label("Em execução:");
        VBox.setMargin(labelCores, new Insets(10, 0, 0, 10));

        HBox coresBox = new HBox();
        VBox.setMargin(coresBox, new Insets(10, 10, 0, 10));

        Label labelAptosP0 = new Label("Aptos P0:");
        VBox.setMargin(labelAptosP0, new Insets(10, 0, 0, 10));

        HBox aptosBoxP0 = new HBox();
        VBox.setMargin(aptosBoxP0, new Insets(10, 10, 0, 10));

        Label labelAptosP1 = new Label("Aptos P1:");
        VBox.setMargin(labelAptosP1, new Insets(10, 0, 0, 10));

        HBox aptosBoxP1 = new HBox();
        VBox.setMargin(aptosBoxP1, new Insets(10, 10, 0, 10));

        Label labelAptosP2 = new Label("Aptos P2:");
        VBox.setMargin(labelAptosP2, new Insets(10, 0, 0, 10));

        HBox aptosBoxP2 = new HBox();
        VBox.setMargin(aptosBoxP2, new Insets(10, 10, 0, 10));

        Label labelAptosP3 = new Label("Aptos P3:");
        VBox.setMargin(labelAptosP3, new Insets(10, 0, 0, 10));

        HBox aptosBoxP3 = new HBox();
        VBox.setMargin(aptosBoxP3, new Insets(10, 10, 0, 10));

        Label labelFinalizados = new Label("Finalizados:");
        VBox.setMargin(labelFinalizados, new Insets(10, 0, 0, 10));

        HBox finalizadosBox = new HBox();
        VBox.setMargin(finalizadosBox, new Insets(10, 10, 0, 10));

        Label labelAbortados = new Label("Abortados:");
        VBox.setMargin(labelAbortados, new Insets(10, 0, 0, 10));

        HBox abortadosBox = new HBox();
        VBox.setMargin(abortadosBox, new Insets(10, 10, 0, 10));

        RR escalonadorRR = new RR(aptosBoxP0, aptosBoxP1, aptosBoxP2, aptosBoxP3, abortadosBox, finalizadosBox);

        Button adicionarProcesso = new Button("Adicionar Processo");
        adicionarProcesso.setOnAction(event -> escalonadorRR.addProcessoDinamico());
        adicionarProcesso.setDisable(true);

        VBox.setMargin(adicionarProcesso, new Insets(10, 0, 0, 10));

        vBox.getChildren().addAll(BaseBox.build(Escalonadores.ROUND_ROBIN), adicionarProcesso, labelMemoria, memoriaBox, labelBlocosQuick, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso, labelCores, coresBox, labelAptosP0, aptosBoxP0, labelAptosP1, aptosBoxP1, labelAptosP2, aptosBoxP2, labelAptosP3, aptosBoxP3, labelFinalizados, finalizadosBox, labelAbortados, abortadosBox);

        scrollPane.setContent(vBox);

        ObjectsManager.getInstance().getButton(models.Button.BUTTON_RR).setOnAction(event -> {
            int numCores = Integer.valueOf(ObjectsManager.getInstance().getField(Field.CORES_VALUE__RR).getText());
            int numProcessosIniciais = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_RR).getText());
            int quantum = Integer.valueOf(ObjectsManager.getInstance().getField(Field.QUANTUM_VALUE_RR).getText());
            int qtdeMemoria = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_QTDE_MEMORIA_RR).getText());
            int filasQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_FILA_QUICK_RR).getText());
            int processosQuick = Integer.valueOf(ObjectsManager.getInstance().getField(Field.FIELD_PROCESSOS_QUICK_RR).getText());

            BaseGerenciador gerenciador = null;

            if(ObjectsManager.getInstance().getRadio(Radio.BEST_FIT_RR).isSelected())
            {
                gerenciador = new BestFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.QUICK_FIT_RR).isSelected())
            {
                gerenciador = new QuickFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso, labelBlocosQuick , processosQuick, filasQuick);
            }
            else if(ObjectsManager.getInstance().getRadio(Radio.MERGE_FIT_RR).isSelected())
            {
                gerenciador = new MergeFit(qtdeMemoria, memoriaBox, labelMemoriaNA, labelMemoriaAlocada, labelMemoriaEmUso);
            }

            escalonadorRR.setQuantum(quantum);
            escalonadorRR.setGerenciador(gerenciador);

            for(int i = 0; i < numProcessosIniciais; i++){
                escalonadorRR.addProcessoEstatico();
            }

            new CPU(numCores, escalonadorRR, gerenciador, coresBox);

            ObjectsManager.getInstance().getButton(models.Button.BUTTON_RR).setDisable(true);

            adicionarProcesso.setDisable(false);
        });

        return scrollPane;
    }
}
